<?php

	// Modelos
	require_once "models/enlaces.php";
	require_once "models/ingresoModel.php";
	require_once "models/promocionModel.php";
	require_once "models/capacitacionModel.php";
	require_once "models/slideModel.php";
	require_once "models/marcaModel.php";
	require_once "models/productoModel.php";
	require_once "models/sucursalModel.php";

	//  Controllers
	require_once "controllers/templateControllers.php";
	require_once "controllers/enlaces.php";
	require_once "controllers/ingresoController.php";
	require_once "controllers/promocionController.php";
	require_once "controllers/capacitacionController.php";
	require_once "controllers/generalController.php";
	require_once "controllers/slideController.php";
	require_once "controllers/marcaController.php";
	require_once "controllers/productoController.php";
	require_once "controllers/sucursalController.php";

	$template = new TemplateController();
	$template -> template();
?>