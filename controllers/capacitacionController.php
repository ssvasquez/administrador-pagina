<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

class CapacitacionController{

	public static function mostrarCapacitacionController(){

		$respuesta = CapacitacionModels::mostrarCapacitacionModel("capacitacion");

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['status_cap'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editarCapacitacion" onclick="editarCapacitacion('.$item['id_cap'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarCapacitacion('.$item['id_cap'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';

/*
				$eventos = '
				<div class="dropdown">
					<button class="btn btn-secondary dropdown-toggle" type="button" id="botonera" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Editar
					</button>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="botonera">
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#editarCapacitacion" onclick="editarCapacitacion('.$item['id_cap'].')"><span class="fa fa-edit"></span> Editar </a>
						<a class="dropdown-item" href="#" onclick="desactivarCapacitacion('.$item['id_cap'].')"><span class="fa fa-toggle-off"></span> Desactivar </a>
						<a class="dropdown-item" href="#">Editar <span class="fa fa-image"></span></a>
					</div>
				</div>
				';*/
			}else{
				
				//$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				//<button type="button" class="btn btn-success btn-sm" onclick="activarPromocion('.$item['id_cap'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}
			
			$datos['data'][] = array(
				$i, 
				$item['lugar_cap'],
				$item['fecha_cap']." ".$item['mes_cap']." | ". $item['hora_cap'],
				$item['marca_cap'], 
				$item['tema_cap'],
				$item['contacto_cap'] . "	|	" .$item['correo_cap'],
				$eventos
			);

			$i++;
		}
		
		//return $respuesta;
		return $datos;
	}

	/*
	public static function mostrarMarcaController(){
		$respuesta = CapacitacionModels::mostrarMarcasModel("marcas");
		
		foreach ($respuesta as $row => $item){

			echo '<option value="'.$item["nombre"].'">'.$item["nombre"].'</option>';

		}
		
	}

*/

	public static function guardarCapacitacionController()	{

		if (isset($_POST['ciudad'], $_POST['mes'])){

			$imagen = $_FILES['imagen']['tmp_name'];

			$nombreImagen = "";
			// promo_2090911-458.jpg o promo_2090911-458.png
			$aleatorio = date("Ymd")."_".mt_rand(100, 999);

			$dir = "../../../img/cursos/curso/".date("Y")."/".$_POST['ciudad']."/".$_POST['mes'];

			if (!file_exists($dir)) {
				mkdir($dir, 0777, true);
			}

			// Tratar imagenes JPG
			if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

				$ruta = $dir."/".$aleatorio.".jpg";

				$origen = imagecreatefromjpeg($imagen);

				imagejpeg($origen, $ruta);
			}

			// Tratar imagenes PNG
			if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

				$ruta = $dir."/".$aleatorio.".png";

				$origen = imagecreatefrompng($imagen);

				imagepng($origen, $ruta);
			}

			//$nombreImagen = basename($ruta);

			$datosController = array("lugar"	=> $_POST['ciudad'],
									"mes" 		=> $_POST['mes'],
									"fecha" 	=> $_POST['fecha'],
									"hora" 		=> $_POST['hora'],
									"imagen" 	=> substr($ruta, 9),
									"desc"		=> $_POST['descripcion'],
									"marca" 	=> $_POST['marca'],
									"tema" 		=> $_POST['tema'],
									"costo" 	=> $_POST['costo'],
									"contacto" 	=> $_POST['contacto'],
									"correo" 	=> $_POST['correo'],
									"liga" 		=> $_POST['liga'],
									"estatus" 	=> "1"
								);

			$respuesta = CapacitacionModels::guardarCapacitacionModel($datosController, "capacitacion");

			return $respuesta;
		}	
		
	}


	public static function mostrarCapacitacionIDController(){

		$datosController = array("id" => $_POST['idCapacitacion']);

		$respuesta = CapacitacionModels::mostrarCapacitacionIDModel($datosController, "capacitacion");

		return $respuesta;
	}


	public static function editarCapacitacionController(){

		$ruta = "";

		$rutaFinal = "";

		if(isset($_POST["emarca"])){

			if(isset($_FILES["imagenCapacitacion"]["tmp_name"])){

				$imagen = $_FILES["imagenCapacitacion"]["tmp_name"];

				if(!empty($_FILES["imagenCapacitacion"]["name"])){

					$aleatorio = date("Ymd")."_".mt_rand(100, 999);

					$dir = "../../../img/cursos/curso/".date("Y")."/".$_POST['eciudad']."/".$_POST['emes'];

					if (!file_exists($dir)) {
						mkdir($dir, 0777, true);
					}

					if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

						$ruta = $dir."/cap-".$aleatorio.".jpg";
		
						$origen = imagecreatefromjpeg($imagen);
		
						imagejpeg($origen, $ruta);
					}
		
					// Tratar imagenes PNG
					if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){
		
						$ruta = $dir."/cap-".$aleatorio.".png";
		
						$origen = imagecreatefrompng($imagen);
		
						imagepng($origen, $ruta);
					}

					unlink("../../../".$_POST["fotoAntigua"]);

					$rutaFinal = substr($ruta, 9);

				}else{

					$ruta = $_POST["fotoAntigua"];

					$rutaFinal = substr($ruta, 0);
				}

			}

			$datosController = array(
					"id"		=> $_POST['idCapacitacion'],
					"imagen"	=> $rutaFinal,
					"fecha"		=> $_POST['efecha'],
					"hora"		=> $_POST['ehora'],
					"desc"		=> $_POST['edescripcion'],
					"marca"		=> $_POST['emarca'],
					"tema"		=> $_POST['etema'],
					"costo"		=> $_POST['ecosto'],
					"contacto"	=> $_POST['econtacto'],
					"correo"	=> $_POST['ecorreo'],
					"liga"		=> $_POST['eliga']
			);

			$respuesta = CapacitacionModels::editarCapacitacionModel($datosController, "capacitacion");

			return $respuesta;
		}

	}

/*
	public static function activarPromocionController(){
		//if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idPromocion'],
									"estatus" => "1");

			$respuesta = CapacitacionModels::estatusCapacitacionModel($datosController, "promociones");

			return $respuesta;

		//}
	}
*/
	public static function desactivarCapacitacionController(){
		//if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idCapacitacion'],
									"estatus" => "0");

			$respuesta = CapacitacionModels::estatusCapacitacionModel($datosController, "capacitacion");

			return $respuesta;

		//}
	}
	
}