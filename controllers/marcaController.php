<?php 

class MarcaController{

	public static function mostrarMarcaController(){

		$respuesta = MarcaModels::mostrarMarcaModel("marcas");

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['status'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editarMarca" onclick="editarMarca('.$item['id'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarMarca('.$item['id'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" onclick="activarMarca('.$item['id'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}
			//../img/promociones/2018/'.$r['img_peque_promo'].'
			$datos['data'][] = array(
				$i,
				'<img src="../img/marcas/logos/'.$item['imagen'].'" alt="" class="img-fluid">',
				$item['nombre'],
				$item['descripcion'],
				$eventos
			);

			$i++;
		}
		
		return $datos;
	}

	public static function guardarMarcaController(){
	
		if (isset($_POST["nombre"])){

			if(isset($_FILES["imagen"]["tmp_name"])){

				$imagen = $_FILES['imagen']['tmp_name'];

				$nombreImagen = "";

				$dir = "../../../img/marcas/logos/";

				if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

					$ruta = $dir.$_FILES['imagen']['name'];
		
					$origen = imagecreatefromjpeg($imagen);
		
					imagejpeg($origen, $ruta);
				}

				if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

					$ruta = $dir.$_FILES['imagen']['name'];
		
					$origen = imagecreatefrompng($imagen);
		
					imagepng($origen, $ruta);
				}
			}

			$nombreImagen = basename($ruta);

			$datosController = array("imagen" => $nombreImagen,
									"nombre" => $_POST['nombre'],
									"descripcion" => $_POST['descripcion'],
									"estatus" => "1");

			$respuesta = MarcaModels::guardarMarcaModel($datosController, "marcas");

			return $respuesta;
		}
	}


	public static function editarMarcaController(){

		$ruta = "";

		if(isset($_POST["enombre"])){

			if(isset($_FILES["imagenMarca"]["tmp_name"])){

				$imagen = $_FILES["imagenMarca"]["tmp_name"];

				if(!empty($_FILES["imagenMarca"]["name"])){

					$dir = "../../../img/marcas/logos/";

					if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

						$ruta = $dir.$_FILES['imagenMarca']['name'];
			
						$origen = imagecreatefromjpeg($imagen);
			
						imagejpeg($origen, $ruta);
					}

					if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

						$ruta = $dir.$_FILES['imagenMarca']['name'];
			
						$origen = imagecreatefrompng($imagen);
			
						imagepng($origen, $ruta);
					}

					unlink("../../".$_POST["fotoAntigua"]);

				}else{

					$ruta = $_POST["fotoAntigua"];
				}
			}

			$nombreImagen = basename($ruta);

			$datosController = array("imagen" => $nombreImagen,
								"nombre" => $_POST['enombre'],
								"descripcion" => $_POST['edescripcion'],
								"id" => $_POST['idMarca']);

			$respuesta = MarcaModels::editarMarcaModel($datosController, "marcas");

			return $respuesta;
		}

	}


	public static function mostrarMarcaIDController(){

		$datosController = array("id" => $_POST['idMarca']);

        $respuesta = MarcaModels::mostrarMarcaIDModel($datosController, "marcas");

        return $respuesta;
	}


	public static function activarMarcaController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idMarca'],
									"estatus" => "1");

            $respuesta = MarcaModels::estatusMarcaModel($datosController, "marcas");

            return $respuesta;

        //}
	}
	
	public static function desactivarMarcaController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idMarca'],
									"estatus" => "0");

            $respuesta = MarcaModels::estatusMarcaModel($datosController, "marcas");

            return $respuesta;

        //}
    }    
}