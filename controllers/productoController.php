<?php 

class ProductoController{

	public static function mostrarProductoController(){

		$respuesta = ProductoModels::mostrarProductoModel("tab_productos");

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){

			if (empty($item['titulo_prod_img'])){

				$imagen = '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarImagenProducto" onclick="imagenProducto('.$item['id_prod'].')"> <span class="fas fa-upload"></span> Imagen</button>';
			} else{

				$imagen = $item['titulo_prod_img'];
			}

			if (empty($item['titulo_prod_pdf'])){

				$pdf = '<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#registrarPDFProducto" onclick="pdfProducto('.$item['id_prod'].')"> <span class="fas fa-upload"></span> PDF</button>';
			} else{

				$pdf = $item['titulo_prod_pdf'];
			}

			
			if($item['status_prod'] == 1 || $item['status_prod'] == 2) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editarProducto" onclick="editarProducto('.$item['id_prod'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarProducto('.$item['id_prod'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
            }else{
				
				$eventos = '<button type="button" class="btn btn-info btn-sm disabled" disabled"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-success btn-sm" onclick="activarProducto('.$item['id_prod'].')"> <span class="fa fa-toggle-off"></span> A c t i v a r</button>';
			}

			$datos['data'][] = array(
				$i,
				$item['clave_prod'],
				$item['sol_prod'],
				$item['seccion_prod'],
				$item['linea_prod'],
				$item['marca_prod'],
				$imagen,
				$pdf,
				$eventos
			);

			$i++;
		}
		return $datos;
	}

	public static function guardarImagenController(){
		if (isset($_POST["idProductoImg"], $_POST["marcaProductoImg"])){

			if(isset($_FILES["imagen"]["tmp_name"])){

				$imagen = $_FILES["imagen"]["tmp_name"];

				$dir = '../../../img/marcas/productos/'.$_POST["marcaProductoImg"].'/';

				if (!file_exists($dir)) {
					mkdir($dir, 0777, true);
				}	


				if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

					$ruta = $dir.$_FILES['imagen']['name'];
		
					$origen = imagecreatefromjpeg($imagen);
		
					imagejpeg($origen, $ruta);
				}

				if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

					$ruta = $dir.$_FILES['imagen']['name'];
		
					$origen = imagecreatefrompng($imagen);
		
					imagepng($origen, $ruta);
				}

				$nombreImagen = basename($ruta);

				$datosController = array("id_prod" => $_POST['idProductoImg'],
								"marcaimg" => $_POST['marcaProductoImg'],
								"tituloimg" => $nombreImagen);

				$respuesta = ProductoModels::guardarImagen($datosController, "imagen");

				return $respuesta;

			}
		}
	}


	public static function guardarPDFController(){
		if (isset($_POST["idProductoPDF"], $_POST["marcaProductoPDF"])){

			if(isset($_FILES["pdf"]["tmp_name"])){

				$pdf = $_FILES["pdf"]["tmp_name"];

				$directorio = '../../../download/'.$_POST["marcaProductoPDF"];

				if (!file_exists($directorio)) {
					mkdir($directorio, 0777, true);
				}
				
				$archivo = $directorio.'/'. $_FILES['pdf']['name'];

				move_uploaded_file($pdf, $archivo);

				$nombreArchivo = basename($archivo);

				$datosController = array("id_prod" => $_POST['idProductoPDF'],
								"marcapdf" => $_POST['marcaProductoPDF'],
								"titulopdf" => $nombreArchivo);

				$respuesta = ProductoModels::guardarPDF($datosController, "pdf");

				return $respuesta;
			}
		}
	}


	public static function guardarProductoController(){
	
		if (isset($_POST["clave"], $_POST["marca"])){

			$datosController = array("clave"	 => $_POST['clave'],
									"nombre" 	=> $_POST['nombre'],
									"desccorta" => $_POST['desccorta'],
									"desclarga" => $_POST['desclarga'],
									"especificacion" => $_POST['especificaciones'],
									"solucion" 	=> $_POST['solucion'],
									"seccion" 	=> $_POST['seccion'],
									"linea" 	=> $_POST['linea'],
									"marca" 	=> $_POST['marca'],
									"serie" 	=> $_POST['serie'],
									"familia" 	=> $_POST['familia'],
									"estatus" 	=> "1");

			$respuesta = ProductoModels::guardarProductoModel($datosController, "tab_productos");

			return $respuesta;
		}
	}


	public static function editarProductoController(){

		if(isset($_POST["eclave"], $_POST["emarca"])){

			$datosController = array("clave" => $_POST['eclave'],
								"nombre" => $_POST['enombre'],
								"desccorta" => $_POST['edesccorta'],
								"desclarga" => $_POST['edesclarga'],
								"especificacion" => $_POST['eespecificaciones'],
								"solucion" 	=> $_POST['esolucion'],
								"seccion" 	=> $_POST['eseccion'],
								"linea" 	=> $_POST['elinea'],
								"marca" 	=> $_POST['emarca'],
								"serie" 	=> $_POST['eserie'],
								"familia" 	=> $_POST['efamilia'],
								"id" 	=> $_POST['id_producto']);

			$respuesta = ProductoModels::editarProductoModel($datosController, "tab_productos");

			return $respuesta;
		}

	}


	public static function mostrarProductoIDController(){

		$datosController = array("id" => $_POST['idProducto']);

        $respuesta = ProductoModels::mostrarProductoIDModel($datosController, "tab_productos");

        return $respuesta;
	}


	public static function activarProductoController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idProducto'],
									"estatus" => "1");

			$respuesta = ProductoModels::estatusProductoModel($datosController, "tab_productos");

			return $respuesta;

        //}
	}
	
	public static function desactivarProductoController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idProducto'],
									"estatus" => "0");

			$respuesta = ProductoModels::estatusProductoModel($datosController, "tab_productos");

			return $respuesta;

        //}
    }
	
	
	public static function mostrarMarcaController(){
		$respuesta = ProductoModels::mostrarMarcasModel("marcas");
		
        foreach ($respuesta as $row => $item){

            echo '<option value="'.$item["nombre"].'">'.$item["nombre"].'</option>';

        }
		
	}
}