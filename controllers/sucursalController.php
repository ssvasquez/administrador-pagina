<?php 

class SucursalController{

	public static function mostrarSucursalController(){
		$respuesta = SucursalModels::mostrarSucursalModel("sucursales");

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['estatus_suc'] == "Activo") {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editarSucursal" onclick="editarSucursal('.$item['id_suc'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarSucursal('.$item['id_suc'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
			
			} else{
				$eventos = '<button type="button" class="btn btn-success btn-sm" onclick="activarSucursal('.$item['id_suc'].')"> <span class="fa fa-toggle-off"></span> Activar</button>';
			}

			$datos['data'][] = array(
				$i, 
				$item['nom_suc'],
				$item['ciudad_suc'],
				$item['estado_suc'],
				$item['tel_suc'],
				$item['contacto_suc'],
				$item['correo_suc'],
				$item['zona_suc'],
				$eventos
			);
			$i++;
		}
		return $datos;
	}

	public static function mostrarSucursalIDController(){
		$datosController = array("id" => $_POST['idSucursal']);

		$respuesta = SucursalModels::mostrarSucursalIDModel($datosController, "sucursales");

		return $respuesta;
	}

	public static function editarSucursalController(){
		if(isset($_POST["enombre"])){

			$datosController = array(
					"nombre" 	=> $_POST['enombre'],
					"calle" 	=> $_POST['ecalle'],
					"numero" 	=> $_POST['enumero'],
					"cp" 		=> $_POST['ecp'],
					"ciudad" 	=> $_POST['eciudad'],
					"delegacion" => $_POST['edelegacion'],
					"estado" 	=> $_POST['eestado'],
					"colonia" 	=> $_POST['ecolonia'],
					"mapa" 		=> $_POST['emapa'],
					"telefono" 	=> $_POST['etelefono'],
					"contacto" 	=> $_POST['econtacto'],
					"correo" 	=> $_POST['ecorreo'],
					"horario_ent1" 	=> $_POST['ehorario0'],
					"horario_sal1" 	=> $_POST['ehorario1'],
					"horario_ent2" 	=> $_POST['ehorario2'],
					"horario_sal2" 	=> $_POST['ehorario3'],
					"alt" 		=> $_POST['edescripcion'],
					"zona" 		=> $_POST['ezona'],
					"id" => $_POST['idSucursal']);

			$respuesta = SucursalModels::editarSucursalModel($datosController, "sucursales");

			return $respuesta;
		}
	}

	public static function activarSucursalController(){
		$datosController = array(
				"id" 		=> $_POST['idSucursal'],
				"estatus" 	=> "Activo");

		$respuesta = SucursalModels::estatusSucursalModel($datosController, "sucursales");

		return $respuesta;
	}
	
	public static function desactivarSucursalController(){
		$datosController = array(
				"id" 		=> $_POST['idSucursal'],
				"estatus" 	=> "Inactivo");

		$respuesta = SucursalModels::estatusSucursalModel($datosController, "sucursales");

		return $respuesta;
	}
	
	public static function guardarSucursalController(){	
		if(isset($_POST["nombre"])){

			$imagen = (isset($_FILES['imagen'])) ? $_FILES['imagen'] : null;

			if ($imagen) {
				$ext = strtolower(pathinfo($imagen['name'], PATHINFO_EXTENSION));

				if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {

					$destino = "../../../img/sucursales/grande/{$imagen['name']}";
				
					$imgOK = move_uploaded_file($imagen['tmp_name'], $destino);

					if ($imgOK){

						$nombreImagen = basename($destino);

						$datosController = array(
							"nombre"	=>	$_POST['nombre'],
							"calle"		=>	$_POST['calle'],
							"numero"	=>	$_POST['numero'],
							"cp"		=>	$_POST['cp'],
							"ciudad"	=>	$_POST['ciudad'],
							"delegacion"	=>	$_POST['delegacion'],
							"estado"	=>	$_POST['estado'],
							"colonia"	=>	$_POST['colonia'],
							"mapa"		=>	$_POST['mapa'],
							"telefono"	=>	$_POST['telefono'],
							"contacto"	=>	$_POST['contacto'],
							"correo"	=>	$_POST['correo'],
							"horario_ent1"	=>	$_POST['horario0'],
							"horario_sal1"	=>	$_POST['horario1'],
							"horario_ent2"	=>	$_POST['horario2'],
							"horario_sal2"	=>	$_POST['horario3'],
							"alt"		=>	$_POST['descripcion'],
							"estatus"	=>	"Activo",
							"zona"		=>	$_POST['zona'],
							"imagen" => $nombreImagen,
							"imagen2" => $nombreImagen);

						$respuesta = SucursalModels::guardarSucursalModel($datosController, "sucursales");

						return $respuesta;
					}
				}
			}
		}
	}
}