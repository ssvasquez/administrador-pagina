<?php
session_start();

class Ingreso{

	public static function ingresoController(){

		if(isset($_POST["usuarioIngreso"])){

			if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["usuarioIngreso"])&&
			   preg_match('/^[a-zA-Z0-9]+$/', $_POST["passwordIngreso"])){

				$encriptar = crypt($_POST["passwordIngreso"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

				$datosController = array("usuario"=>$_POST["usuarioIngreso"],
										"password"=>$encriptar);

				$respuesta = IngresoModels::ingresoModel($datosController, "usuarios");

				$usuarioActual = $_POST["usuarioIngreso"];

				if($respuesta["nombre_usu"] == $_POST["usuarioIngreso"] && $respuesta["pass_usu"] == $encriptar){

					//session_start();

					$_SESSION["validar"] = true;
					$_SESSION["usuario"] = $respuesta["nombre_usu"];

					//header("location:inicio");
					echo'<script type="text/javascript"> window.location.href="inicio";</script>';
				}

				else{

					echo '<div class="alert alert-danger">Error al ingresar</div>';

				}
			}
		}
	}
}