<?php 

class PromocionController{

	public static function mostrarPromocionController(){

		$respuesta = PromocionModels::mostrarPromocionModel("promociones");

		$datos = array('data' => array());

		$i = 1;
		foreach ($respuesta as $row => $item){
			
			if($item['estatus_promo'] == 1) {

				$eventos =  '<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#editarPromocion" onclick="editarPromocion('.$item['id_promo'].')"> <span class="fa fa-edit"></span> Editar</button>
				<button type="button" class="btn btn-danger btn-sm" onclick="desactivarPromocion('.$item['id_promo'].')"> <span class="fa fa-toggle-off"></span> Desactivar</button>';
			}
			// Temporal: No generar error por campos vacios
			$fecha = ($item['fecha_promo'] != "") ? $item['fecha_promo'] : "N";

			$datos['data'][] = array(
				$i, 
				$item['marca_promo'],
				$item['titulo_promo'],
				$item['desc_promo'],
				$fecha,
				$eventos
			);

			$i++;
		}

		return $datos;
	}

	public static function mostrarMarcaController(){
		$respuesta = PromocionModels::mostrarMarcasModel("marcas");
		
        foreach ($respuesta as $row => $item){

            echo '<option value="'.$item["nombre"].'">'.$item["nombre"].'</option>';

        }
		
	}


	public static function guardarPromocionController(){
	
		if(isset($_POST["marca"], $_POST["titulo"], $_POST['descripcion'])){

			$imagen = $_FILES['imagen']['tmp_name'];

			$nombreImagen = "";
			// promo_2090911-458.jpg o promo_2090911-458.png
			$aleatorio = date("Ymd")."_".mt_rand(100, 999);

			// Tratar imagenes JPG
			if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

				$ruta = "../../../img/promociones/2018/promo_".$aleatorio.".jpg";

				$origen = imagecreatefromjpeg($imagen);

				imagejpeg($origen, $ruta);
			}

			// Tratar imagenes PNG
			if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

				$ruta = "../../../img/promociones/2018/promo_".$aleatorio.".png";

				$origen = imagecreatefrompng($imagen);

				imagepng($origen, $ruta);
			}

			$nombreImagen = basename($ruta);

			$datosController = array("imagen" => $nombreImagen,
									"marca" => $_POST['marca'],
									"titulo" => $_POST['titulo'],
									"descripcion" => $_POST['descripcion'],
									"estatus" => "1",
									"link1" => $_POST['link1'],
									"link2" => $_POST['link2'],
									"fecha" => date("Y-m-d"));

			$respuesta = PromocionModels::guardarPromocionModel($datosController, "promociones");

			return $respuesta;

		}
	}



	public static function editarPromocionController(){

		$ruta = "";

		if(isset($_POST["emarca"])){

			if(isset($_FILES["imagenPromo"]["tmp_name"])){

				$imagen = $_FILES["imagenPromo"]["tmp_name"];

				if(!empty($_FILES["imagenPromo"]["name"])){

					$dir = "../../../img/promociones/2018/";

					$aleatorio = date("Ymd")."_".mt_rand(100, 999);

					if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

						$ruta = $dir."promo_".$aleatorio.".jpg";
			
						$origen = imagecreatefromjpeg($imagen);
			
						imagejpeg($origen, $ruta);
					}

					if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

						$ruta = $dir."promo_".$aleatorio.".png";
			
						$origen = imagecreatefrompng($imagen);
			
						imagepng($origen, $ruta);
					}

					// Eliminar imagen antigua | 
					if (file_exists("../../".$_POST["fotoAntigua"])) {
						unlink("../../".$_POST["fotoAntigua"]);
					}

				}else{

					$ruta = $_POST["fotoAntigua"];

				}
			}

			$nombreImagen = basename($ruta);

			$datosController = array("imagen" => $nombreImagen,
								"marca" => $_POST['emarca'],
								"titulo" => $_POST['etitulo'],
								"descripcion" => $_POST['edescripcion'],
								"link1" => $_POST['elink1'],
								"link2" => $_POST['elink2'],
								"id" => $_POST['idPromocion']);

			$respuesta = PromocionModels::editarPromocionModel($datosController, "promociones");

			return $respuesta;
		}

	}


	public static function mostrarPromocionIDController(){

		$datosController = array("id" => $_POST['idPromocion']);

		$respuesta = PromocionModels::mostrarPromocionIDModel($datosController, "promociones");

		return $respuesta;
	}


	public static function activarPromocionController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idPromocion'],
									"estatus" => "1");

            $respuesta = PromocionModels::estatusPromocionModel($datosController, "promociones");

            return $respuesta;

        //}
	}
	
	public static function desactivarPromocionController(){
        //if(isset($_GET["idDesactivar"])){
			
			$datosController = array("id" => $_POST['idPromocion'],
									"estatus" => "0");

            $respuesta = PromocionModels::estatusPromocionModel($datosController, "promociones");

            return $respuesta;

        //}
    }
}