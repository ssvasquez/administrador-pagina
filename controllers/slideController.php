<?php 

class SlideController{	

	public static function mostrarSlideController(){		

		$respuesta = SlideModel::mostrarSlideModel("slides");
		
		foreach($respuesta as $row => $item){

			if ($item["status_slide"] == 1){
				
				echo '<li id="'.$item["id_slide"].'" class="bloqueSlide">
					<span class="fas fa-pencil-alt " onclick="editarSlide('.$item['id_slide'].')"></span>
					<img src="../img/home/'.$item["imagen_slide"].'" class="handleImg">
				</li>';
			}			
		}
	}


	public static function mostrarSlideIDController(){

		$datosController = array("id"	=>	$_POST['idSlide']);

		$respuesta = SlideModel::mostrarSlideIDModel($datosController, "slides");

		return $respuesta;
	}


	public static function editarSlideController(){

		$ruta = "";

		if(isset($_POST["emarca"]) && !empty($_POST["emarca"])){

				//if(isset($_FILES["imagenSlide"])){
			if(isset($_FILES["imagenSlide"]["tmp_name"])){

				$imagen = $_FILES["imagenSlide"]["tmp_name"];

				if(!empty($_FILES["imagenSlide"]["name"])){ 

					$dir = "../../../img/home";

					//$aleatorio = mt_rand(100, 999);
					$aleatorio = date("Ymd")."_".mt_rand(100, 999);

					if (exif_imagetype( $imagen ) == IMAGETYPE_JPEG){

						$ruta = $dir."/banner-".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($imagen);

						$destino = imagecrop($origen, ["x"=>0, "y"=>0, "width"=>1170, "height"=>390]);

						imagejpeg($destino, $ruta);
					}

					// Tratar imagenes PNG
					if (exif_imagetype( $imagen ) == IMAGETYPE_PNG){

						$ruta = $dir."/banner-".$aleatorio.".png";

						$origen = imagecreatefrompng($imagen);

						$destino = imagecrop($origen, ["x"=>0, "y"=>0, "width"=>1170, "height"=>390]);

						imagepng($destino, $ruta);
					}
					
					// Eliminar banner antigua | 
					if (file_exists("../../".$_POST["fotoAntigua"])) {
						unlink("../../".$_POST["fotoAntigua"]);
					}

					//unlink("../../".$_POST["fotoAntigua"]);

				} else{

					$ruta = $_POST["fotoAntigua"];
					
				}
			}

			$nombreImagen = basename($ruta);
			
			// Cargar los datos para enviar al Model.
			$datosController = array("id"=>$_POST["idSlide"],
									"marca"=>$_POST["emarca"],
									"descripcion"=>$_POST["edescripcion"],
									"ruta"=> $nombreImagen,
									"liga"=>$_POST["eliga"]);
									 
			$respuesta = SlideModel::editarSlideModel($datosController, "slides");
			
			return $respuesta;
		}
	}
}