<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Admin CDC Group</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="views/images/fav.png" type="image/png" />
		<link rel="stylesheet" href="views/css/all.min.css">	<!-- Font Awesome -->
		
		<link rel="stylesheet" href="views/css/dataTables.bootstrap4.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">		
		<link rel="stylesheet" href="views/css/sweetalert.css">
		<link rel="stylesheet" href="views/css/plugins/fileinput.min.css">
		<link rel="stylesheet" href="views/css/plugins/theme.min.css">
		<link rel="stylesheet" href="views/css/adminlte.css">
		<link rel="stylesheet" href="views/css/style2.css">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">	<!-- Google Font: Source Sans Pro -->
	</head>

	<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
				
				<?php
					$modulos = new Enlaces();
					$modulos -> enlacesController();
				?>
		
		<script src="views/js/jquery.min.js"></script>	<!-- jQuery -->		
		<script src="views/js/bootstrap.bundle.min.js"></script><!-- Bootstrap 4 -->		
		<script src="views/js/jquery.dataTables.js"></script>
		<script src="views/js/dataTables.bootstrap4.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js "></script>
		<script src="https://cdn.ckeditor.com/4.13.0/basic/ckeditor.js"></script>

		<script src="views/js/plugins/sweetalert.min.js"></script>
		<script src="views/js/plugins/piexif.min.js"></script>
		<script src="views/js/plugins/fileinput.min.js"></script>
		<script src="views/js/plugins/theme.js"></script>
		<script src="views/js/plugins/themefas.js"></script>
		<script src="views/js/plugins/locales/es.js"></script>

		<script src="views/js/adminlte.min.js"></script>	<!-- AdminLTE App -->		
		<script src="views/js/demo.js"></script><!-- AdminLTE for demo purposes -->

		<script src="views/js/app/promocion.js"></script>
		<script src="views/js/app/validarIngreso.js"></script>
		<script src="views/js/app/capacitacion.js"></script>
		<script src="views/js/app/slide.js"></script>
		<script src="views/js/app/marca.js"></script>
		<script src="views/js/app/producto.js"></script>
		<script src="views/js/app/sucursal.js"></script>
	</body>
</html>