<?php
error_reporting(E_ALL);

ini_set('display_errors', '1');

require_once "../../controllers/capacitacionController.php";
require_once "../../models/capacitacionModel.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxCapacitacion{


	public function mostrarCapacitacionAjax(){
		
		$respuesta = CapacitacionController::mostrarCapacitacionController();
		
		print json_encode($respuesta);

	}

	public function mostrarCapacitacionID(){

		$respuesta = CapacitacionController::mostrarCapacitacionIDController();
		print json_encode($respuesta);	

	}


	public function guardarCapacitacion(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CapacitacionController::guardarCapacitacionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La capacitación se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar de alta capacitación";
		}

		echo json_encode($salida);
	}

	/*
	public function editarImagen(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CapacitacionController::editarImagenPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La imagen ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar actualizar la imagen";
		}
		echo json_encode($salida);

	}
*/
	public function editarCapacitacion(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = CapacitacionController::editarCapacitacionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Capacitación ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar capacitación";
		}
	
		echo json_encode($salida);

	}


	public function desactivarCapacitacion(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = CapacitacionController::desactivarCapacitacionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Capacitación desactivado correctamente";

		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar capacitación";

		}

		echo json_encode($salida);
	}
/*
	public function activarPromocion(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$respuesta = CapacitacionController::activarPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Promoción activado correctamente";

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar promoción";

		}

		echo json_encode($salida);
	}
*/

}

if($opcion == 'mostrar'){
	$mostrar = new AjaxCapacitacion();
	$mostrar -> mostrarCapacitacionAjax();
}


if($opcion == 'getID'){
	$id = new AjaxCapacitacion();
	$id -> mostrarCapacitacionID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxCapacitacion();
	$guardar -> guardarCapacitacion();
}

if ($opcion == 'editar'){
	$editar = new AjaxCapacitacion();
	$editar -> editarCapacitacion();
}

if ($opcion == 'desactivar'){
	$desactivar = new AjaxCapacitacion();
	$desactivar -> desactivarCapacitacion();
}

if ($opcion == 'activar'){
	$activar = new AjaxCapacitacion();
	$activar -> activarPromocion();
}

if ($opcion == 'editarimagen'){
	$editarImagen = new AjaxCapacitacion();
	$editarImagen -> editarImagen();
}