<?php

require_once "../../controllers/promocionController.php";
require_once "../../models/promocionModel.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxPromocion{


	public function mostrarPromocionAjax(){
		

		$respuesta = PromocionController::mostrarPromocionController();
		
		print json_encode($respuesta);

	}

	public function mostrarPromocionID(){

		$respuesta = PromocionController::mostrarPromocionIDController();
		print json_encode($respuesta);	
	}


	public function guardarPromocion(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = PromocionController::guardarPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La promoción se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar de alta promoción";
		}

		echo json_encode($salida);
	}

	public function editarImagen(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = PromocionController::editarImagenPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La imagen ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar actualizar la imagen";
		}
		echo json_encode($salida);

	}

	public function editarPromocion(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = PromocionController::editarPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La promoción ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar actualizar la promoción";
		}
	
		echo json_encode($salida);

	}


	public function desactivarPromocion(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = PromocionController::desactivarPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Promoción desactivado correctamente";

		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar promoción";

		}

		echo json_encode($salida);
	}


	public function activarPromocion(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$respuesta = PromocionController::activarPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Promoción activado correctamente";

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar promoción";

		}

		echo json_encode($salida);
	}


}

if($opcion == 'mostrar'){
	$mostrar = new AjaxPromocion();
	$mostrar -> mostrarPromocionAjax();
}

if($opcion == 'getID'){
	$idCategoria = new AjaxPromocion();
	$idCategoria -> mostrarPromocionID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxPromocion();
	$guardar -> guardarPromocion();
}

if ($opcion == 'editar'){
	$editar = new AjaxPromocion();
	$editar -> editarPromocion();
}

if ($opcion == 'desactivar'){
	$desactivarCat = new AjaxPromocion();
	$desactivarCat -> desactivarPromocion();
}

if ($opcion == 'activar'){
	$desactivarCat = new AjaxPromocion();
	$desactivarCat -> activarPromocion();
}

if ($opcion == 'editarimagen'){
	$editarImagen = new AjaxPromocion();
	$editarImagen -> editarImagen();
}



