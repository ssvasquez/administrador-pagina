<?php

require_once "../../controllers/productoController.php";
require_once "../../models/productoModel.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxProducto{


	public function mostrarProductoAjax(){
		

		$respuesta = ProductoController::mostrarProductoController();
		
		print json_encode($respuesta);

	}

	public function mostrarProductoID(){

		$respuesta = ProductoController::mostrarProductoIDController();
		print json_encode($respuesta);	
	}


	public function guardarProducto(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = ProductoController::guardarProductoController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La marca se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar de alta la marca";
		}

		echo json_encode($salida);
	}

	public function guardarImagen(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = ProductoController::guardarImagenController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La imagen se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al subir la imagen";
		}

		echo json_encode($salida);
	}


	public function guardarPDF(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = ProductoController::guardarPDFController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "El PDF se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al subir PDF";
		}

		echo json_encode($salida);
	}

	
	public function editarProducto(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = ProductoController::editarProductoController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "El producto ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar producto";
		}
	
		echo json_encode($salida);

	}


	public function desactivarProducto(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = ProductoController::desactivarProductoController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Producto desactivado correctamente";

		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar producto";

		}

		echo json_encode($salida);
	}


	public function activarProducto(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$respuesta = ProductoController::activarProductoController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Marca activado correctamente";

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar marca";

		}

		echo json_encode($salida);
	}


}

if($opcion == 'mostrar'){
	$mostrar = new AjaxProducto();
	$mostrar -> mostrarProductoAjax();
}

if($opcion == 'getID'){
	$id = new AjaxProducto();
	$id -> mostrarProductoID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxProducto();
	$guardar -> guardarProducto();
}


if($opcion == 'imagen'){
	$imagen = new AjaxProducto();
	$imagen -> guardarImagen();
}

if($opcion == 'pdf'){
	$pdf = new AjaxProducto();
	$pdf -> guardarPDF();
}


if ($opcion == 'editar'){
	$editar = new AjaxProducto();
	$editar -> editarProducto();
}

if ($opcion == 'desactivar'){
	$desactivar = new AjaxProducto();
	$desactivar -> desactivarProducto();
}

if ($opcion == 'activar'){
	$activar = new AjaxProducto();
	$activar -> activarProducto();
}




