<?php


require_once "../../models/slideModel.php";
require_once "../../controllers/slideController.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

#CLASE Y MÉTODOS
#-------------------------------------------------------------

class AjaxSlide{

	#SUBIR LA IMAGEN DEL SLIDE
	#----------------------------------------------------------

	/*
	public $nombreImagen;
	public $imagenTemporal;



	//public $imagenTemporal;

	public function gestorSlide1Ajax(){

		$datos = $this->imagenTemporal;

		$respuesta = SlideController::mostrarImagenNuevaController($datos);

		echo $respuesta;

	}*/


	#ACTUALIZAR ORDEN
	#---------------------------------------------
	/*
	public $actualizarOrdenSlide;
	public $actualizarOrdenItem;

	public function actualizarOrdenAjax(){

		$datos = array("ordenSlide" => $this->actualizarOrdenSlide,
			           "ordenItem" => $this->actualizarOrdenItem);

		$respuesta = SlideController::actualizarOrdenController($datos);

		echo $respuesta;

	}*/

	

	public function mostrarSlideID(){

		$respuesta = SlideController::mostrarSlideIDController();

		echo json_encode($respuesta);
		
		# Recuperar error en JSON 
		if (json_last_error() != JSON_ERROR_NONE) {
			printf("JSON Error: %s", json_last_error_msg());
		}

	}


	
 

	public function editarSlide(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = SlideController::editarSlideController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Slide actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar slider";
		}

		echo json_encode($salida);
	}

}

if($opcion == 'getID'){
	$id = new AjaxSlide();
	$id -> mostrarSlideID();
}

if ($opcion == 'editar'){
	$editar = new AjaxSlide();
	$editar -> editarSlide();
}



#OBJETOS
#-----------------------------------------------------------

/*
// EDICION
if(isset($_FILES["imagenedicion"]["tmp_name"])){

	$a = new AjaxSlide();
	$a -> imagenTemporal = $_FILES["imagenedicion"]["tmp_name"];
	$a -> gestorSlide1Ajax();

}*/

/*
if(isset($_POST["actualizarOrdenSlide"])){

	$d = new AjaxSlide();
	$d -> actualizarOrdenSlide = $_POST["actualizarOrdenSlide"];
	$d -> actualizarOrdenItem = $_POST["actualizarOrdenItem"];
	$d -> actualizarOrdenAjax();

}*/