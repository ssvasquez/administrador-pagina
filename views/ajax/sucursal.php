<?php

require_once "../../controllers/sucursalController.php";
require_once "../../models/sucursalModel.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxSucursal{


	public function mostrarSucursalAjax(){

		$respuesta = SucursalController::mostrarSucursalController();
		
		print json_encode($respuesta);

	}


	public function mostrarSucursalID(){

		$respuesta = SucursalController::mostrarSucursalIDController();
		print json_encode($respuesta);	
	}


	public function guardarSucursal(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = SucursalController::guardarSucursalController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La sucursal se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar de alta la sucursal";
		}

		echo json_encode($salida);
	}

	public function editarImagen(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = SucursalController::editarImagenPromocionController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La imagen ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar actualizar la imagen";
		}
		echo json_encode($salida);

	}

	public function editarSucursal(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = SucursalController::editarSucursalController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La sucursal ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar la sucursal";
		}
	
		echo json_encode($salida);

	}


	public function desactivarSucursal(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = SucursalController::desactivarSucursalController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Sucursal desactivado correctamente";

		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar sucursal";

		}

		echo json_encode($salida);
	}


	public function activarSucursal(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$respuesta = SucursalController::activarSucursalController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Sucursal activado correctamente";

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar sucursal";

		}

		echo json_encode($salida);
	}
}



if($opcion == 'mostrar'){
	$mostrar = new AjaxSucursal();
	$mostrar -> mostrarSucursalAjax();
}

if($opcion == 'getID'){
	$id = new AjaxSucursal();
	$id -> mostrarSucursalID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxSucursal();
	$guardar -> guardarSucursal();
}

if ($opcion == 'editar'){
	$editar = new AjaxSucursal();
	$editar -> editarSucursal();
}

if ($opcion == 'desactivar'){
	$desactivar = new AjaxSucursal();
	$desactivar -> desactivarSucursal();
}

if ($opcion == 'activar'){
	$activar = new AjaxSucursal();
	$activar -> activarSucursal();
}

if ($opcion == 'editarimagen'){
	$editarImagen = new AjaxSucursal();
	$editarImagen -> editarImagen();
}