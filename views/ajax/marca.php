<?php

require_once "../../controllers/marcaController.php";
require_once "../../models/marcaModel.php";

$opcion = isset($_GET['op'])?$_GET['op']:'';

class AjaxMarca{


	public function mostrarMarcaAjax(){
		

		$respuesta = MarcaController::mostrarMarcaController();
		
		print json_encode($respuesta);

	}

	public function mostrarMarcaID(){

		$respuesta = MarcaController::mostrarMarcaIDController();
		print json_encode($respuesta);	
	}


	public function guardarMarca(){
		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = MarcaController::guardarMarcaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La marca se dio de alta correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al dar de alta la marca";
		}

		echo json_encode($salida);
	}

	
	public function editarMarca(){

		$salida = array('success' => false, 'mensaje' => array());
		$respuesta = MarcaController::editarMarcaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "La marca ha sido actualizado correctamente";
		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al actualizar la marca";
		}
	
		echo json_encode($salida);

	}


	public function desactivarMarca(){

		$salida = array('success' => false, 'mensaje' => array());

		$respuesta = MarcaController::desactivarMarcaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Marca desactivada correctamente";

		}else{
			$salida['success'] = false;
			$salida['mensaje'] = "Error al desactivar la marca";

		}

		echo json_encode($salida);
	}


	public function activarMarca(){

		$salida = array('success' => false, 'mensaje' => array());
		
		$respuesta = MarcaController::activarMarcaController();

		if ($respuesta === TRUE)
		{
			$salida['success'] = true;
			$salida['mensaje'] = "Marca activado correctamente";

		}else{

			$salida['success'] = false;
			$salida['mensaje'] = "Error al activar marca";

		}

		echo json_encode($salida);
	}


}

if($opcion == 'mostrar'){
	$mostrar = new AjaxMarca();
	$mostrar -> mostrarMarcaAjax();
}

if($opcion == 'getID'){
	$idCategoria = new AjaxMarca();
	$idCategoria -> mostrarMarcaID();
}

if($opcion == 'guardar'){
	$guardar = new AjaxMarca();
	$guardar -> guardarMarca();
}

if ($opcion == 'editar'){
	$editar = new AjaxMarca();
	$editar -> editarMarca();
}

if ($opcion == 'desactivar'){
	$desactivarCat = new AjaxMarca();
	$desactivarCat -> desactivarMarca();
}

if ($opcion == 'activar'){
	$desactivarCat = new AjaxMarca();
	$desactivarCat -> activarMarca();
}




