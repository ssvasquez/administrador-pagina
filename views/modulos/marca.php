			<?php
				if (session_status() != 2){
					session_start(); 
				}

				if(!$_SESSION["validar"]){

					//header("location:ingreso");
					echo'<script type="text/javascript"> window.location.href="ingreso";</script>';

					exit();
				}
			?>

			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<hr>
					<section class="content">
						<div class="row">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Lista de marcas activas</h3>
										<div class="text-right">
											<button id="btnAgregarMarca" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarMarca"><i class="fas fa-plus"></i> Agregar</button>
										</div>
									</div>
								<!-- /.card-header -->
									<div class="card-body">
										<table id="marcaCDC" class="table table-striped" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>#</th>
													<th>Logo</th>
													<th>Nombre</th>
													<th>Descripcion</th>
													<th></th>
												</tr>
												</thead>
										</table>
									</div><!-- /.card-body -->
								</div><!-- /.card -->
							</div><!-- /.col -->
						</div>
					</section>				<!-- /.content -->

					<div id="registrarMarca" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Nueva Marca</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarMarca" enctype="multipart/form-data">
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<div class="file-loading">
														<label for="imagen">Imagen de la marca *</label>
														<input type="file" class="form-control" id="subirFoto" name="imagen" >
													</div>
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="nombre">Nombre marca *</label>
													<input type="text" class="form-control" id="nombre" name="nombre" placeholder="nombre marca">
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="descripcion">Descripción *</label>
													<textarea id="descripcion" name="descripcion"  class="form-control" cols="30" rows="2"></textarea>
												</div>
											</div>

										</div>

										<div class="form-group">
											<div class="text-right">
												<button type="submit" class="btn btn-primary"> Registrar	</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<div id="editarMarca" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Editar Marca</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form role="form" method="POST" action="" id="frmEditarMarca">
										<div class="row">
											<div class="col-md-6">
												<input type="file" id="imagenMarcaActual" name="imagenMarca">
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="enombre">Nombre marca <sapn class="text-danger">*</span></label>
													<input type="hidden" class="form-control" id="id_marca" name="idMarca">
													<input type="hidden" class="form-control" id="fotoAntigua" name="fotoAntigua">
													<input type="text" class="form-control" id="enombre" name="enombre" >
												</div>

												<div class="form-group">
													<label for="edescripcion">Descripción *</label>
													<textarea id="edescripcion" name="edescripcion"  class="form-control" cols="30" rows="2"></textarea>
												</div>

												<div class="form-group">
													<div class="text-right">
														<button type="submit" class="btn btn-primary"> Actualizar </button>
													</div>
												</div>

											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
			</div>