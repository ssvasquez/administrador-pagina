				<?php					
					if (session_status() != 2){
						session_start(); 
					}
					 
					if(!$_SESSION["validar"]){

						//header("location:ingreso");
						echo'<script type="text/javascript"> window.location.href="ingreso";</script>';

						exit();
					}
				?>

				<style>
					.krajee-default.file-preview-frame .kv-file-content{
						width:445px;
					}

					.krajee-default .file-footer-caption{
						padding-top: 0px !important;
						margin-bottom: 5px !important;
					}
				</style>
				<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<hr>

				<div id="imgSlide" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pt-1">
					<p><span class="fa fa-arrow-down"></span>Imagen (tamaño recomendado: 1170px * 390px)</p>
					<ul id="columnasSlide">
						<?php
							$slide = new SlideController();
							$slide -> mostrarSlideController();
						?>
					</ul>
				</div>

				<hr>

				<section id="editarSlide" class="mt-2" style="display:none">
					<form action="" id="frmEditarSlide" enctype="multipart/form-data">
						<div class="container py-3">
							<div class="row">
								<div class="col-12 col-md-6">
									<input type="file" id="imagenSlideActual" name="imagenSlide">
								</div>
								<div class="col-12 col-md-6">
									 <img src="" class="img-fluid" alt="" id="img-actual">

									<input type="hidden" id="idSlide" name="idSlide">
									<input type="hidden" id="fotoAntigua" name="fotoAntigua">

									<div class="form-group">
										<label for="emarca" class="m-0">Marca</label>
										<input type="text" class="form-control" id="emarca" name="emarca">
									</div>
									<div class="form-group">
										<label for="edescripcion" class="m-0">Descripción </label>
										<textarea id="edescripcion" name="edescripcion"  class="form-control" cols="30" rows="1"></textarea>
									</div>	
									<div class="form-group">
										<label for="eliga" class="m-0">Liga ó URL *</label>
										<input type="text" class="form-control" id="eliga" name="eliga" placeholder="">
									</div>
									<div class="text-right">
										<button type="submit" class="btn btn-primary">Guardar Cambios</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</section>

				</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
			</div>