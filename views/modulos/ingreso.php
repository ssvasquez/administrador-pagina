	<div class="login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href=""><b>Admin</b>CDCGroup</a>
			</div>
		<!-- /.login-logo -->
			<div class="card">
				<div class="card-body login-card-body">
					<p class="login-box-msg">Iniciar Sesion</p>
					<form action="#" method="post" id="formIngreso" onsubmit="return validarIngreso()">
						<div class="input-group mb-3">
							<input type="text" class="form-control" id="usuarioIngreso" name="usuarioIngreso" placeholder="Usuario">
							<div class="input-group-append">
								<div class="input-group-text"><span class="fas fa-user"></span></div>
							</div>
						</div>

						<div class="input-group mb-3">
							<input type="password" id="passwordIngreso" name="passwordIngreso" class="form-control" placeholder="Password">
							<div class="input-group-append">
								<div class="input-group-text"><span class="fas fa-lock"></span></div>
							</div>
						</div>

						<?php

							$ingreso = new Ingreso();
							$ingreso -> ingresoController();
							
						?>

						<div class="row">
							<div class="col-12">
								<button type="submit" class="btn btn-primary btn-block formIngreso">Iniciar Sesion</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	