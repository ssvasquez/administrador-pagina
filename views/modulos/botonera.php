			<aside class="main-sidebar sidebar-dark-primary elevation-4">
				<!-- Brand Logo -->
				<a href="http://cdcmx.com/" class="brand-link" target="_blank">
					<img src="views/images/cdcgroup.png"	alt="CDC Group"	class="brand-image img-circle "	style="opacity: .8">
					<span class="brand-text font-weight-light">CDC Group</span>
				</a>

				<!-- Sidebar -->
				<div class="sidebar">
				<!-- Sidebar user (optional) -->
					<div class="user-panel mt-3 pb-3 mb-3 d-flex">
						<div class="image">
							<img src="views/images/user.png" class="img-circle elevation-2" alt="User Image">
						</div>
						<div class="info">
							<a href="salir" class="d-block text-capitalize"><?php echo $_SESSION["usuario"];?></a>
						</div>
					</div>

				<!-- Sidebar Menu -->
					<nav class="mt-2">
						<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">					
							<li class="nav-item">
								<a href="inicio" class="nav-link">
									<i class="nav-icon fas fa-home"></i>
									<p>	Inicio</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="slide" class="nav-link">
									<i class="nav-icon fas fa-images"></i>
									<p>	Slider</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="capacitacion" class="nav-link">
									<i class="nav-icon fas fa-laptop"></i>
									<p>	Capacitación </p>
								</a>
							</li>
							<li class="nav-item">
								<a href="promocion" class="nav-link">
									<i class="nav-icon fas fa-dollar-sign"></i>
									<p>	Promociones	</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="marca" class="nav-link">
									<i class="nav-icon fab fa-elementor"></i>
									<p>	Marcas	</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="producto" class="nav-link">
									<i class="nav-icon fab fa-product-hunt"></i>
									<p>	Productos	</p>
								</a>
							</li>

							<li class="nav-item">
								<a href="sucursal" class="nav-link">
									<i class="nav-icon fas fa-map-marker-alt"></i>
									<p>	Sucursales	</p>
								</a>
							</li>

						</ul>
					</nav>
				<!-- /.sidebar-menu -->
				</div>
				<!-- /.sidebar -->
			</aside>