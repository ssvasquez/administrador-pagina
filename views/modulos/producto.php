<?php
				if (session_status() != 2){
					session_start(); 
				}

				if(!$_SESSION["validar"]){

					echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
					//header("location:ingreso");

					exit();
				}
			?>

			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<hr>
					<section class="content">
						<div class="row">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Lista de productos activos</h3>
										<div class="text-right">
											<button id="btnAgregarProducto" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarProducto"><i class="fas fa-plus"></i> Agregar</button>
										</div>
									</div>
								<!-- /.card-header -->
									<div class="card-body">
										<table id="producto" class="table table-striped" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>#</th>
													<th>Clave</th>
													<!--<th>Nombre</th>
													<th>Descripcion</th>-->
													<th>Solucion</th>
													<th>Seccion</th>
													<th>Linea</th>
													<th>Marca</th>
													<!--<th>Serie</th>-->
													<th>Imagen</th>
													<th>PDF</th>
													<th></th>
												</tr>
											</thead>
										</table>
									</div><!-- /.card-body -->
								</div><!-- /.card -->
							</div><!-- /.col -->
						</div>
					</section>				<!-- /.content -->

					<div id="registrarProducto" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Nuevo Producto</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarProducto" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label for="clave" class="m-0">Clave *</label>
													<input type="text" class="form-control" id="clave" name="clave" >
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">	
													<label for="nombre" class="m-0">Nombre *</label>
													<input type="text" class="form-control" id="nombre" name="nombre" >
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="desccorta" class="m-0">Breve descripción *</label>
													<input type="text" id="desccorta" name="desccorta"  class="form-control">
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="desclarga" class="m-0">Descripcion completa *</label>
													<!--<input id="desclarga" name="desclarga" type="hidden">
													<div id="text-descripcion" class="text-especial" style="height: 90px">-->
													<textarea name="desclarga" id="desclarga" rows="3" ></textarea>	
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="especificaciones" class="m-0">Especificaciones *</label>
													<!--<input id="especificaciones" name="especificaciones" type="hidden">
													<div id="text-especificaciones" class="text-especial" style="height: 110px">-->
													<textarea name="especificaciones" id="especificaciones" rows="4" ></textarea>	
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="marca" class="m-0">Marca *</label>
													<select class="form-control" name="marca" id="marca">
													<option value="">Seleccione...</option>
														<?php
															$marca = new ProductoController();
															$marca -> mostrarMarcaController();
														?>	
													</select>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="solucion" class="m-0">Soluciones *</label>
													<select name="solucion" id="solucion" class="form-control">
														<option value="">Seleccione...</option>
														<option value="Administración de cableado">Administración de Cableado</option>
														<option value="Cableado y Fibra Optica">Cableado Estructurado y Fibra Optica</option>
														<option value="Herramientas y Equipo De Medición">Equipo de Medicion</option>
														<option value="Control de Iluminación">Control de Iluminacion</option>
														<option value="Networking">Networking</option>
														<option value="Respaldo De Energía">Respaldo de Energia</option>
														<option value="Seguridad">Seguridad</option>
														<option value="Soluciones de Voz">Soluciones de Voz</option>
														<option value="Videocolaboración">Videocolaboración</option>
													</select>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="seccion" class="m-0">Sección *</label>
													<input type="text" class="form-control" id="seccion" name="seccion" >
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="linea" class="m-0">Linea *</label>
													<input type="text" class="form-control" id="linea" name="linea" >
												</div>
											</div>
											

											<div class="col-md-6">
												<div class="form-group">
													<label for="serie" class="m-0">Serie *</label>
													<input type="text" class="form-control" id="serie" name="serie" >
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="form-group">
													<label for="familia" class="m-0">Familia * <small> Aplica para algunas marcas</small></label>
													<input type="text" class="form-control" id="familia" name="familia" >
												</div>
											</div>

										</div>

										<div class="form-group">
											<div class="text-right">
												<button type="submit" class="btn btn-primary"> Registrar	</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

					<div id="registrarImagenProducto" class="modal fade">
						<div class="modal-dialog modal-md" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Imagen Producto</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarImagenProducto" enctype="multipart/form-data">
										<div class="row">
											<div class="col-12">
												<input type="text" id="idProductoImg" name="idProductoImg">
												<input type="text" id="marcaProductoImg" name="marcaProductoImg">
												<div class="form-group">
													<label for="imagen" class="m-0">Imagen del producto*</label>
													<input type="file" name="imagen" class="form-control" id="imagenProducto">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="text-right">
												<button type="submit" class="btn btn-primary"> Subir Imagen	</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

					<div id="registrarPDFProducto" class="modal fade">
						<div class="modal-dialog modal-md" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">PDF Producto</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarPDFProducto" enctype="multipart/form-data">
										<div class="row">
											<div class="col-12">
												<input type="text" id="idProductoPDF" name="idProductoPDF">
												<input type="text" id="marcaProductoPDF" name="marcaProductoPDF">
												<div class="form-group">
													<label for="pdf" class="m-0">PDF del producto*</label>
													<input type="file" name="pdf" class="form-control" id="pdfProducto">
												</div>
											</div>
										</div>

										<div class="form-group">
											<div class="text-right">
												<button type="submit" class="btn btn-primary"> Subir PDF	</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<div id="editarProducto" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Editar Producto</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmEditarProducto" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-6">
												<input type="hidden" id="id_producto" name="id_producto">
												<div class="form-group">
													<label for="eclave" class="m-0">Clave *</label>
													<input type="text" class="form-control" id="eclave" name="eclave" >
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">	
													<label for="enombre" class="m-0">Nombre *</label>
													<input type="text" class="form-control" id="enombre" name="enombre" >
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="edesccorta" class="m-0">Breve descripción *</label>
													<textarea id="edesccorta" name="edesccorta"  class="form-control"  rows="2"></textarea>
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="edesclarga" class="m-0">Descripcion completa *</label>
													<textarea id="edesclarga" name="edesclarga"  class="form-control"  rows="2"></textarea>
												</div>
											</div>

											<div class="col-12">
												<div class="form-group">
													<label for="eespecificaciones" class="m-0">Especificaciones *</label>
													<textarea id="eespecificaciones" name="eespecificaciones" cols="30" rows="10"></textarea>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="emarca" class="m-0">Marca *</label>
													<select class="form-control" name="emarca" id="emarca">
													<option value="">Seleccione...</option>
														<?php
															$marca = new ProductoController();
															$marca -> mostrarMarcaController();
														?>	
													</select>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="esolucion" class="m-0">Soluciones *</label>
													<select name="esolucion" id="esolucion" class="form-control">
														<option value="">Seleccione...</option>
														<option value="Administración de cableado">Administración de Cableado</option>
														<option value="Cableado y Fibra Optica">Cableado Estructurado y Fibra Optica</option>
														<option value="Herramientas y Equipo De Medición">Equipo de Medicion</option>
														<option value="Control de Iluminación">Control de Iluminacion</option>
														<option value="Networking">Networking</option>
														<option value="Respaldo De Energía">Respaldo de Energía</option>
														<option value="Seguridad">Seguridad</option>
														<option value="Soluciones de Voz">Soluciones de Voz</option>
														<option value="Videocolaboración">Videocolaboración</option>
													</select>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="eseccion" class="m-0">Sección *</label>
													<input type="text" class="form-control" id="eseccion" name="eseccion" >
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group">
													<label for="elinea" class="m-0">Linea *</label>
													<input type="text" class="form-control" id="elinea" name="elinea" >
												</div>
											</div>
											

											<div class="col-md-6">
												<div class="form-group">
													<label for="eserie" class="m-0">Serie *</label>
													<input type="text" class="form-control" id="eserie" name="eserie" >
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="form-group">
													<label for="efamilia" class="m-0">Familia * <small> Aplica para algunas marcas</small></label>
													<input type="text" class="form-control" id="efamilia" name="efamilia" >
												</div>
											</div>

										</div>

										<div class="form-group">
											<div class="text-right">
												<button type="submit" class="btn btn-primary"> Actualizar producto	</button>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

				</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
			</div>