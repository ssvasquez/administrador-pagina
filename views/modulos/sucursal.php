<?php
				if (session_status() != 2){
					session_start(); 
				}

				if(!$_SESSION["validar"]){
					//header("location:ingreso");
					echo'<script type="text/javascript"> window.location.href="ingreso";</script>';
					exit();
				}

			?>

			<style>
				.krajee-default.file-preview-frame .kv-file-content{
					width: auto !important;
					height: auto !important;
				}
			</style>

			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<hr>
					<section class="content">
						<div class="row">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Lista de sucursales CDC Group</h3>
										<div class="text-right">
											<button id="btnAgregarSucursal" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarSucursal"><i class="fas fa-plus"></i> Agregar</button>
										</div>
									</div>
								<!-- /.card-header -->
									<div class="card-body">
										<table id="sucursal" class="table table-striped" width="100%" cellspacing="0">
											<thead>
												<tr>	
													<th>#</th>
													<th>Nombre</th>
													<th>Ciudad</th>
													<th>Estado</th>
													<th>Telefono</th>
													<th>Contacto</th>
													<th>Correo</th>
													<th>Zona</th>
													<th></th>
												</tr>
												</thead>
										</table>
									</div><!-- /.card-body -->
								</div><!-- /.card -->
							</div><!-- /.col -->
						</div>
					</section>				<!-- /.content -->

					<div id="registrarSucursal" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Nueva Sucursal</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarSucursal" enctype="multipart/form-data">
										<div class="row">
											<div class="col-12">
												<div class="form-group">
													<div class="file-loading">
														<label for="imagen">Imagen de la sucursal *</label>
														<input type="file" class="form-control" id="subirFoto" name="imagen" >
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-5">
															<label for="nombre">Nombre *</label>
															<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre sucursal">
														</div>
														<div class="col-md-7">
															<label for="descripcion">A L T (Descripción) *</label>
															<input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción sucursal">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="calle">Calle *</label>
															<input type="text" class="form-control" id="calle" name="calle" placeholder="Calle">
														</div>
														<div class="col-md-3">
															<label for="numero">Numero *</label>
															<input type="text" class="form-control" id="numero" name="numero" placeholder="Numero">
														</div>

														<div class="col-md-3">
															<label for="cp">Codigo Postal *</label>
															<input type="text" class="form-control" id="cp" name="cp" placeholder="Codigo Postal">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="estado">Estado *</label>
															<input type="text" class="form-control" id="estado" name="estado" placeholder="Estado">
														</div>
														<div class="col-md-6">
															<label for="ciudad">Ciudad *</label>
															<input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="delegacion">Delegación / Municipio *</label>
															<input type="text" class="form-control" id="delegacion" name="delegacion" placeholder="Delegación / Municipio">
														</div>
														<div class="col-md-6">
															<label for="colonia">Colonia *</label>
															<input type="text" class="form-control" id="colonia" name="colonia" placeholder="Colonia">
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="mapa">Mapa *</label>
													<textarea id="mapa" name="mapa"  class="form-control" cols="30" rows="3"></textarea>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-4">
															<label for="contacto">Contacto *</label>
															<input type="text" class="form-control" id="contacto" name="contacto" placeholder="Nombre contacto">
														</div>

														<div class="col-md-4">
															<label for="correo">Correo *</label>
															<input type="text" class="form-control" id="correo" name="correo" placeholder="Correo contacto">
														</div>

														<div class="col-md-4">
															<label for="telefono">Telefono *</label>
															<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono contacto">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="horario">Horario Lun - Vie *</label>
															<div class="row">
																<div class="col-md-6">
																	<select class="form-control" name="horario0" id="horario0">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>

																<div class="col-md-6">
																	<select class="form-control" name="horario1" id="horario1">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<label for="horario2">Horario Sabado *</label>
															<div class="row">
																<div class="col-md-6">
																	<select class="form-control" name="horario2" id="horario2">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>

																<div class="col-md-6">
																	<select class="form-control" name="horario3" id="horario3">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="zona">Zona *</label>
													<select class="form-control" name="zona" id="zona">
														<option value="">Seleccione...</option>
														<option value="CDMX">CDMX</option>
														<option value="Foraneo">Foraneo</option>
													</select>
												</div>

												<div class="form-group">
													<div class="text-right">
														<button type="submit" class="btn btn-primary"> Registrar	</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<div id="editarSucursal" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Editar Sucursal</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form role="form" method="POST" action="" id="frmEditarSucursal">
										<div class="row">
											<div class="col-12">

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="enombre">Nombre *</label>
															<input type="hidden" class="form-control" id="id_sucursal" name="idSucursal">
															<input type="text" class="form-control" id="enombre" name="enombre" placeholder="Nombre sucursal">
														</div>
														<div class="col-md-6">
															<label for="edescripcion">A L T (Descripción) *</label>
															<input type="text" class="form-control" id="edescripcion" name="edescripcion" placeholder="Descripción sucursal">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="ecalle">Calle *</label>
															<input type="text" class="form-control" id="ecalle" name="ecalle" placeholder="Calle">
														</div>
														<div class="col-md-3">
															<label for="enumero">Numero *</label>
															<input type="text" class="form-control" id="enumero" name="enumero" placeholder="Numero">
														</div>

														<div class="col-md-3">
															<label for="ecp">Codigo Postal *</label>
															<input type="text" class="form-control" id="ecp" name="ecp" placeholder="CodigoPostal">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="eestado">Estado *</label>
															<input type="text" class="form-control" id="eestado" name="eestado" placeholder="Estado">
														</div>
														<div class="col-md-6">
															<label for="eciudad">Ciudad *</label>
															<input type="text" class="form-control" id="eciudad" name="eciudad" placeholder="Ciudad">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label for="edelegacion">Delegación / Municipio *</label>
															<input type="text" class="form-control" id="edelegacion" name="edelegacion" placeholder="Delegación / Municipio">
														</div>
														<div class="col-md-6">
															<label for="ecolonia">Colonia *</label>
															<input type="text" class="form-control" id="ecolonia" name="ecolonia" placeholder="Colonia">
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="emapa">Mapa *</label>
													<textarea id="emapa" name="emapa"  class="form-control" cols="30" rows="5"></textarea>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-4">
															<label for="econtacto">Contacto *</label>
															<input type="text" class="form-control" id="econtacto" name="econtacto" placeholder="Nombre Contacto">
														</div>

														<div class="col-md-4">
															<label for="ecorreo">Correo *</label>
															<input type="text" class="form-control" id="ecorreo" name="ecorreo" placeholder="Correo Contacto">
														</div>

														<div class="col-md-4">
															<label for="etelefono">Telefono *</label>
															<input type="text" class="form-control" id="etelefono" name="etelefono" placeholder="Telefono sucursal">
														</div>
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-md-6" style="background-color:#E0E0E0">
															<label for="ehorario">Horario Lun - Vie *</label>
															<div class="row">
																<div class="col-md-6">
																	<select class="form-control" name="ehorario0" id="ehorario0">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>

																<div class="col-md-6">
																	<select class="form-control" name="ehorario1" id="ehorario1">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<label for="ehorario2">Horario Sabado *</label>
															<div class="row">
																<div class="col-md-6">
																	<select class="form-control" name="ehorario2" id="ehorario2">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>

																<div class="col-md-6">
																	<select class="form-control" name="ehorario3" id="ehorario3">
																		<?php
																			$horario = new GeneralController();
																			$horario -> horarioController();
																		?>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="form-group">
													<label for="ezona">Zona *</label>
													<select class="form-control" name="ezona" id="ezona">
														<option value="">Seleccione...</option>
														<option value="CDMX">CDMX</option>
														<option value="Foraneo">Foraneo</option>
													</select>
												</div>

												<div class="form-group">
													<div class="text-right">
														<button type="submit" class="btn btn-primary"> Actualizar</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<div id="editarImagenSucursal" class="modal fade">
						<div class="modal-dialog modal-sm" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Editar Imagen Sucursal</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form role="form" method="POST" action="" id="frmEditarImagenSucursal">
										<div class="row">
											<input type="hidden" class="form-control" id="id_sucursal" name="idSucursal">
											<input type="hidden" class="form-control" id="fotoAntigua" name="fotoAntigua">

											<div class="col-md-12">
												<input type="file" id="imagenSucursalActual" name="imagenSucursal">
											</div>

											<div class="form-group">
												<div class="text-right">
													<button type="submit" class="btn btn-primary"> Actualizar Imagen</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


				</div>	<!-- /.content-wrapper -->
				<?php include "views/modulos/footer.php"; ?>
			</div>