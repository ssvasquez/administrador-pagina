			<?php
				if (session_status() != 2){
					session_start(); 
				}

				if(!$_SESSION["validar"]){

					//header("location:ingreso");
					echo'<script type="text/javascript"> window.location.href="ingreso";</script>';

					exit();
				}
			?>
			
			<style>
				.krajee-default.file-preview-frame .kv-file-content{
					width: auto !important;
					height: auto !important;
				}
			</style>

			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<hr>
				<section class="content">
					<div class="row">
					  	<div class="col-12">
							<div class="card">
						  		<div class="card-header">
									<h3 class="card-title">Capacitacion</h3>
									<div class="text-right">
										<button id="btnAgregarCapacitacion" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarCapacitacion"><i class="fas fa-plus"></i> Agregar</button>
									</div>
						  		</div>
						  <!-- /.card-header -->
						  		<div class="card-body">
									<table id="capacitacion" class="table table-striped" width="100%" cellspacing="0">
							  			<thead>
							  				<tr>
												<th>#</th>
												<th>Lugar</th>
												<th>Fecha - Hora</th>
												<th>Marca</th>
												<th>Tema</th>
												<th>Contacto</th>
												<th></th>
											</tr>
							  			</thead>
									</table>
								</div><!-- /.card-body -->
							</div><!-- /.card -->
						</div><!-- /.col -->
					</div>
				</section>

				<div id="registrarCapacitacion" class="modal fade">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<p class="lead">Nueva Capacitación</p>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="frmRegistrarCapacitacion" enctype="multipart/form-data">
									<div class="row">

										<div class="col-md-6">
											<div class="form-group">
												<label for="imagen" class="m-0">Imagen del Curso*</label>
												<input type="file" name="imagen" class="form-control" id="fotoCapacitacion">
											</div>
										</div>

										<div class="col-md-6">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="ciudad" class="m-0">Ciudad *</label>
														<select name="ciudad" id="ciudad" class="form-control">
															<option value="">Seleccione ciudad...</option>
															<option value="CDMX">CDMX</option>
															<option value="Cer">Certificaciones</option>
															<option value="CHH">Chihuahua</option>
															<option value="GUA">Guadalajara</option>
															<option value="HER">Hermosillo</option>
															<option value="LEO">Leon</option>
															<option value="MER">Merida</option>
															<option value="MTY">Monterrey</option>
															<option value="QRO">Queretaro</option>
															<option value="PUE">Puebla</option>
															<option value="VER">Veracruz</option>
															<option value="Webex">Webex</option>
														</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label for="mes" class="m-0">Mes *</label>
														<select name="mes" id="mes" class="form-control">
															<option value="">Seleccione mes...</option>
															<?php
															$meses = new GeneralController();
															$meses -> mesesController();
															?>
														</select>
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label for="fecha" class="m-0">Dia(s) * </label><small class="text-muted"> Eje: 01 y 02</small>
														<input type="text" class="form-control" id="fecha" name="fecha" placeholder="" required="true">
													</div>
												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label for="hora" class="m-0">Hora *</label><small class="text-muted">Eje: 10:00 a 12:00</small>
														<input type="text" class="form-control" id="hora" name="hora" placeholder="Horario del curso">
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="tema" class="m-0">Tema *</label>
														<input type="text" class="form-control" id="tema" name="tema" placeholder="Tema del curso">
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="descripcion" class="m-0">Descripcion *</label>
														<textarea id="descripcion" name="descripcion"  class="form-control" cols="30" rows="2"></textarea>
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="marca" class="m-0">Marca *</label>
														<select class="form-control" name="marca" id="marca">
														<option value="">Seleccione...</option>
															<?php
															$marca = new PromocionController();
															$marca -> mostrarMarcaController();
															?>	
														</select>
														<!--<input type="text" class="form-control" id="marca" name="marca" placeholder="Marca">-->
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="costo" class="m-0">Costo *</label>
														<input type="text" class="form-control" id="costo" name="costo" placeholder="Costo del curso">
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="contacto" class="m-0">Contacto *</label>
														<input type="text" class="form-control" id="contacto" name="contacto" placeholder="Nombre del contacto">
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="correo" class="m-0">Correo *</label>
														<input type="text" class="form-control" id="correo" name="correo" placeholder="Correo del contacto">
													</div>
												</div>

												<div class="col-12">
													<div class="form-group">
														<label for="liga"> Liga </label>
														<input type="text" class="form-control" id="liga" name="liga" placeholder="Liga del curso">
													</div>
												</div>
											</div>		

											<div class="form-group">
												<div class="text-right">
													<button type="submit" class="btn btn-primary"> Registrar	</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->


				<div id="editarCapacitacion" class="modal fade">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<p class="lead">Editar Capacitación</p>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="frmEditarCapacitacion" enctype="multipart/form-data">
									<div class="row">
										<div class="col-md 6">
											<input type="file" id="imagenCapacitacionActual" name="imagenCapacitacion">
										</div>

										<div class="col-md 6">
											<input type="hidden" class="form-control" id="id_capacitacion" name="idCapacitacion">
											<input type="hidden" class="form-control" id="fotoAntigua" name="fotoAntigua">
											<input type="hidden" class="form-control" id="eciudad" name="eciudad">
											<input type="hidden" class="form-control" id="emes" name="emes">

											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="efecha" class="m-0">Dia(s) * </label><small class="text-muted"> Eje: 01 y 02</small>
														<input type="text" class="form-control" id="efecha" name="efecha" placeholder="" required="true">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="ehora" class="m-0">Hora *</label><small class="text-warning">Eje: 10:00 a 12:00</small>
														<input type="text" class="form-control" id="ehora" name="ehora" placeholder="Horario del curso">
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="etema" class="m-0">Tema *</label>
												<input type="text" class="form-control" id="etema" name="etema" placeholder="Tema del curso">
											</div>

											<div class="form-group">
												<label for="edescripcion" class="m-0">Descripcion *</label>
												<textarea id="edescripcion" name="edescripcion"  class="form-control" cols="30" rows="2"></textarea>
											</div>

											<div class="form-group">
												<label for="emarca" class="m-0">Marca *</label>
												<select class="form-control" name="emarca" id="emarca">
													<?php
													$marca = new PromocionController();
													$marca -> mostrarMarcaController();
													?>	
												</select>
											</div>

											<div class="form-group">
												<label for="ecosto" class="m-0">Costo *</label>
												<input type="text" class="form-control" id="ecosto" name="ecosto" placeholder="Costo del curso">
											</div>

											<div class="form-group">
												<label for="econtacto" class="m-0">Contacto *</label>
												<input type="text" class="form-control" id="econtacto" name="econtacto" placeholder="Nombre del contacto">
											</div>

											<div class="form-group">
												<label for="ecorreo" class="m-0">Correo *</label>
												<input type="text" class="form-control" id="ecorreo" name="ecorreo" placeholder="Correo del contacto">
											</div>

											<div class="form-group">
												<label for="eliga"> Liga </label>
												<input type="text" class="form-control" id="eliga" name="eliga" placeholder="Liga del curso">
											</div>

											<div class="form-group">
												<div class="text-right">
													<button type="submit" class="btn btn-primary"> Actualizar	</button>
												</div>
											</div>

										</div>
									</div>
								</form>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->


			</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
		</div>