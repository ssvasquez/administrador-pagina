<?php
			if (session_status() != 2){
				session_start(); 
			}

			if(!$_SESSION["validar"]){

				//header("location:ingreso");
				echo'<script type="text/javascript"> window.location.href="ingreso";</script>';

				exit();
			}
			?>
			
			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<hr>
				<section class="content" >
					<div class="container-fluid">
						<div class="row">
							<div class="col-12 py-4">
								<div class="text-center text-secondary">
									<h1 class="display-4">Panel de Administración CDC Group</h1>
								</div>
							</div>
						</div>

						
						<div class="row">

							<div class="col-lg-3 col-6">
								<div class="small-box bg-info">
									<div class="inner">
										<h3 class="">12</h3>
										<p>Slider</p>
									</div>
									<div class="icon">
										<i class="fas fa-images"></i>
									</div>
									<a href="slide" class="small-box-footer"> <i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>

							<!-- ./col -->
							<div class="col-lg-3 col-6">
								<div class="small-box bg-success">
									<div class="inner">
										<h3>...</h3>
										<p>Capacitación</p>
									</div>
									<div class="icon">
										<i class="fas fa-laptop"></i>
									</div>
									<a href="capacitacion" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>

							<!-- ./col -->
							<div class="col-lg-3 col-6">
								<div class="small-box bg-warning">
									<div class="inner">
										<h3>...</h3>
										<p>Promoción</p>
									</div>
									<div class="icon">
										<i class="fas fa-dollar-sign"></i>
									</div>
									<a href="promocion" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>
							
							
							<div class="col-lg-3 col-6">
								<div class="small-box bg-danger">
									<div class="inner">
										<h3>...</h3>
										<p>Marcas</p>
									</div>
									<div class="icon">
										<i class="nav-icon fab fa-elementor"></i>
									</div>
									<a href="marca" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>
							
							
							
							<div class="col-lg-3 col-6">
								<div class="small-box bg-primary">
									<div class="inner">
										<h3>...</h3>
										<p>Productos</p>
									</div>
									<div class="icon">
										<i class="nav-icon fab fa-product-hunt"></i>
									</div>
									<a href="producto" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>


							<div class="col-lg-3 col-6">
								<div class="small-box bg-warning">
									<div class="inner">
										<h3>...</h3>
										<p>Sucursales</p>
									</div>
									<div class="icon">
										<i class="nav-icon fas fa-map-marker-alt"></i>
									</div>
									<a href="sucursal" class="small-box-footer"><i class="fas fa-arrow-circle-right"></i></a>
								</div>
							</div>

						</div><!-- /.row -->
					</div><!-- /.container-fluid -->
				</section>
			</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
		</div>	