			<?php
				if (session_status() != 2){
					session_start(); 
				}

				if(!$_SESSION["validar"]){

					//header("location:ingreso");
					echo'<script type="text/javascript"> window.location.href="ingreso";</script>';

					exit();
				}

			?>

			<style>
				.krajee-default.file-preview-frame .kv-file-content{
					width: auto !important;
					height: auto !important;
				}
			</style>

			<div class="wrapper">
			<!-- Navbar -->
			<?php include "views/modulos/cabecera.php"; ?>
			<!-- /.navbar -->

			<!-- Main Sidebar Container -->
			<?php include "views/modulos/botonera.php"; ?>

			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					<hr>
					<section class="content">
						<div class="row">
							<div class="col-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Lista de promociones activas</h3>
										<div class="text-right">
											<button id="btnAgregarPromocion" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#registrarPromocion"><i class="fas fa-plus"></i> Agregar</button>
										</div>
									</div>
								<!-- /.card-header -->
									<div class="card-body">
										<table id="promocionBack" class="table table-striped" width="100%" cellspacing="0">
											<thead>
												<tr>
													<th>#</th>
													<th>Marca promo</th>
													<th>Titulo</th>
													<th>Descripcion</th>
													<th>Fecha</th>
													<th></th>
												</tr>
												</thead>
										</table>
									</div><!-- /.card-body -->
								</div><!-- /.card -->
							</div><!-- /.col -->
						</div>
					</section>				<!-- /.content -->

					<div id="registrarPromocion" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Nueva Promoción</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmRegistrarPromocion" enctype="multipart/form-data">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<div class="file-loading">
														<label for="imagen">Imagen de la promocion *</label>
														<input type="file" class="form-control" id="subirFoto" name="imagen" >
													</div>
												</div>
											</div>


											<div class="col-md-6">
												<div class="form-group">
													<label for="marca">Marca *</label>
													<select class="form-control" name="marca" id="marca">
													<option value="">Seleccione...</option>
														<?php
															$marca = new PromocionController();
															$marca -> mostrarMarcaController();
														?>	
													</select>
												</div>

												<div class="form-group">
													<label for="titulo">Titulo promoción *</label>
													<input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo promoción">
												</div>

												<div class="form-group">
													<label for="descripcion">Descripción *</label>
													<textarea id="descripcion" name="descripcion"  class="form-control" cols="30" rows="2"></textarea>
												</div>

												<div class="form-group">
													<label for="link1">Link 1 </label> <small class="text-muted"> Opcional</small>
													<input type="text" class="form-control" id="link1" name="link1" placeholder="Link 1">
												</div>

												<div class="form-group">
													<label for="link2">Link 2  </label><small class="text-muted"> Opcional</small>
													<input type="text" class="form-control" id="link2" name="link2" placeholder="Link 2">
												</div>

												<div class="form-group">
													<div class="text-right">
														<button type="submit" class="btn btn-primary"> Registrar	</button>
													</div>
												</div>

											</div>

										</div>										
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->


					<div id="editarPromocion" class="modal fade">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<p class="lead">Editar Promoción</p>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form role="form" method="POST" action="" id="frmEditarPromocion">
										<div class="row">
											<div class="col-md-6">
												<input type="file" id="imagenPromoActual" name="imagenPromo">
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label for="marca">Marca *</label>
													<input type="hidden" class="form-control" id="id_promo" name="idPromocion">
													<input type="hidden" class="form-control" id="fotoAntigua" name="fotoAntigua">
													<select class="form-control" name="emarca" id="emarca">
														<?php
															$marca = new PromocionController();
															$marca -> mostrarMarcaController();
														?>	
													</select>
												</div>

												<div class="form-group">
													<label for="titulo">Titulo promoción *</label>
													<input type="text" class="form-control" id="etitulo" name="etitulo" placeholder="Titulo promoción">
												</div>

												<div class="form-group">
													<label for="descripcion">Descripción *</label>
													<textarea id="edescripcion" name="edescripcion"  class="form-control" cols="30" rows="4"></textarea>
												</div>

												<div class="form-group">
													<label for="elink1">Link 1 </label> <span class="text-muted"> Opcional</span>
													<input type="text" class="form-control" id="elink1" name="elink1" placeholder="Link 1">
												</div>

												<div class="form-group">
													<label for="elink2">Link 2 </label> <span class="text-muted"> Opcional</span>
													<input type="text" class="form-control" id="elink2" name="elink2" placeholder="Link 2">
												</div>

												<div class="form-group">
													<div class="text-right">
														<button type="submit" class="btn btn-primary"> Actualizar </button>
													</div>
												</div>

											</div>
										</div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</div>	<!-- /.content-wrapper -->
			<?php include "views/modulos/footer.php"; ?>
			</div>