// format para 
$("#subirFoto").fileinput({
	theme:'fas',
	browseClass: "btn btn-primary btn-block",
	allowedFileExtensions: ['jpg', 'png'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false
});


var tblSucursal;

/* =======================================
    AREA PARA OPERACIONES PROMOCION.
=======================================*/

/*	=======	M O S T R A R Promocion	=======*/ 
tblSucursal = $("#sucursal").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/sucursal.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Sucursal	=======*/
$("#btnAgregarSucursal").on('click', function () {

	$("#frmRegistrarSucursal")[0].reset();

	$("#frmRegistrarSucursal").unbind('submit').bind('submit', function () {

		var formData = new FormData(document.getElementById("frmRegistrarSucursal"));
		var imagenSuc = $('#subirFoto')[0].files[0];
		formData.append('imagen', imagenSuc);

		$.ajax({
			url: "views/ajax/sucursal.php?op=guardar",
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (response) {
				if (response.success == true){
					$("#frmRegistrarSucursal")[0].reset();
					tblSucursal.ajax.reload(null, false);
					$("#registrarSucursal").modal('hide');
					successSwal(response.mensaje);
				}else{
					$("#registrarSucursal").modal('hide');
					errorSwal(response.mensaje);
				}
			}
		});
		return false;
	});
});


/*	=======	E D I T A R Sucursal	=======*/
function editarSucursal(idSucursal) {
	$("#frmEditarSucursal")[0].reset();
	if (idSucursal) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/sucursal.php?op=getID",
			type: 'POST',
			data: {
				idSucursal: idSucursal
			},
			dataType: 'json',
			success: function (response) {

				$("#id_sucursal").val(response.id_suc);
				$("#enombre").val(response.nom_suc);
				$("#edescripcion").val(response.alt_sucursal);
				$("#ecalle").val(response.calle_suc);
				$("#enumero").val(response.num_suc);
				$("#ecp").val(response.cp_suc);
				$("#eestado").val(response.estado_suc);
				$("#eciudad").val(response.ciudad_suc);
				$("#edelegacion").val(response.deleg_mun_suc);
				$("#ecolonia").val(response.col_suc);
				$("#emapa").val(response.map_suc);
				$("#econtacto").val(response.contacto_suc);
				$("#ecorreo").val(response.correo_suc);
				$("#etelefono").val(response.tel_suc);
				$("#ehorario0").val(response.horario_ent_suc);
				$("#ehorario1").val(response.horario_sal_suc);
				$("#ehorario2").val(response.horario_ent_sab_suc);
				$("#ehorario3").val(response.horario_sal_sab_suc);
				$("#ezona").val(response.zona_suc);

				$("#frmEditarSucursal").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarSucursal"));
					//var imagenPromo = $('#imagenPromoActual')[0].files[0];
					//formData.append('imagenPromo', imagenPromo);

					//if (marca && titulo && descripcion) {
					$.ajax({
						url: "views/ajax/sucursal.php?op=editar",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblSucursal.ajax.reload(null, false);
								$("#editarSucursal").modal('hide');
								successSwal(response.mensaje);

							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#editarSucursal").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					//}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	E D I T A R Imagen Sucursal	=======*/
function editarImagenSucursal(idSucursal) {
	$("#frmEditarImagenSucursal")[0].reset();
	if (idSucursal) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/sucursal.php?op=getImagenID",
			type: 'POST',
			data: {
				idSucursal: idSucursal
			},
			dataType: 'json',
			success: function (response) {

				$("#imagenSucursalActual")
					.fileinput("destroy")
					.fileinput({
					theme:'fas',
					browseClass: "btn btn-primary btn-block",
					allowedFileExtensions: ['jpg', 'png'],
					browseLabel: "Examinar nueva imagen sucursal...",
					maxFilesNum: 1,
					language: 'es',
					showCaption: false,
					showRemove: false,
					showUpload: false,
					showCancel: false,
					initialPreview: [
						`<img class='img-fluid' src='../img/sucursales/grande/${response.img_suc}'>`
					],
					initialPreviewConfig: [{
						caption: response.imagen_slide
					}]
				});
				$("#fotoAntigua").val(`../img/promociones/2018/${response.img_suc}`);

				$("#id_sucursal").val(response.id_suc);

				$("#frmEditarImagenSucursal").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarImagenSucursal"));
					var imagenSucursal = $('#imagenSucursalActual')[0].files[0];
					formData.append('imagenPromo', imagenSucursal);
					
					//if (marca && titulo && descripcion) {
					$.ajax({
						url: "views/ajax/sucursal.php?op=editarImagen",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblSucursal.ajax.reload(null, false);
								$("#editarSucursal").modal('hide');
								successSwal(response.mensaje);

							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#editarSucursal").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					//}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}



/*	=======	A C T I V A R  Sucursal	=======*/
function activarSucursal(idSucursal = null){
	if(idSucursal){
		swal({
			title: "¿Realmente desea A C T I V A R esta sucursal?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/sucursal.php?op=activar",
					type: 'post',
					data: {idSucursal : idSucursal},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblSucursal.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Sucursal	=======*/
function desactivarSucursal(idSucursal = null){
	if(idSucursal){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta sucursal?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/sucursal.php?op=desactivar",
					type: 'post',
					data: {idSucursal : idSucursal},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblSucursal.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}



/** Mensajes de Acierto y Error. */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}