
$("#subirFoto").fileinput({
	theme:'fas',
	browseClass: "btn btn-primary btn-block",
	allowedFileExtensions: ['jpg', 'png'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false
});


var tblMarca;

/* =======================================
    AREA PARA OPERACIONES MARCA.
=======================================*/

/*	=======	M O S T R A R Marca	=======*/ 
tblMarca = $("#marcaCDC").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/marca.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Marca	=======*/
$("#btnAgregarMarca").on('click', function () {

	$("#frmRegistrarMarca")[0].reset();

	$("#frmRegistrarMarca").unbind('submit').bind('submit', function () {

		var formData = new FormData(document.getElementById("frmRegistrarMarca"));
		var imagenMarca = $('#subirFoto')[0].files[0];
		formData.append('imagen', imagenMarca);

		$.ajax({
			url: "views/ajax/marca.php?op=guardar",
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (response) {
				if (response.success == true){
					$("#frmRegistrarMarca")[0].reset();
					tblMarca.ajax.reload(null, false);
					$("#registrarMarca").modal('hide');
					successSwal(response.mensaje);
				}else{
					$("#registrarMarca").modal('hide');
					errorSwal(response.mensaje);
				}
			}
		});
		//}
		//errorSwal("Inserte datos en el formulario");
		return false;
	});
});


/*	=======	E D I T A R Marca	=======*/
function editarMarca(idMarca) {
	$("#frmEditarMarca")[0].reset();
	if (idMarca) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/marca.php?op=getID",
			type: 'POST',
			data: {
				idMarca: idMarca
			},
			dataType: 'json',
			success: function (response) {

				$("#imagenMarcaActual")
					.fileinput("destroy")
					.fileinput({

					theme:'fas',
					browseClass: "btn btn-primary btn-block",
					allowedFileExtensions: ['jpg', 'png'],
					browseLabel: "Examinar nueva imagen marca...",
					maxFilesNum: 1,
					language: 'es',
					showCaption: false,
					showRemove: false,
					showUpload: false,
					showCancel: false,
					initialPreview: [
						`<img class='img-fluid' src='../img/marcas/logos/${response.imagen}'>`
					],
					initialPreviewConfig: [{
						caption: response.nombre
					}]
				});
				$("#fotoAntigua").val(`../img/marcas/logos/${response.imagen}`);
				$("#id_marca").val(response.id);
				$("#enombre").val(response.nombre);
				$("#edescripcion").val(response.descripcion);

				$("#frmEditarMarca").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarMarca"));
					var imagenMarca = $('#imagenMarcaActual')[0].files[0];
					formData.append('imagenMarca', imagenMarca);
					//if (marca && titulo && descripcion) {
					$.ajax({
						url: "views/ajax/marca.php?op=editar",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblMarca.ajax.reload(null, false);
								$("#editarMarca").modal('hide');
								successSwal(response.mensaje);

							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#editarMarca").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					//}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Marca	=======*/
function activarMarca(idMarca = null){
	if(idMarca){
		swal({
			title: "¿Realmente desea A C T I V A R esta marca?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/marca.php?op=activar",
					type: 'post',
					data: {idMarca : idMarca},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblMarca.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Marca	=======*/
function desactivarMarca(idMarca = null){
	if(idMarca){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta marca?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/marca.php?op=desactivar",
					type: 'post',
					data: {idMarca : idMarca},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblMarca.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}



/** Mensajes de Acierto y Error. */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}