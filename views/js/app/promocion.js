
$("#subirFoto").fileinput({
	theme:'fas',
	browseClass: "btn btn-primary btn-block",
	allowedFileExtensions: ['jpg', 'png'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false
});


var tblPromocion;

/* =======================================
    AREA PARA OPERACIONES PROMOCION.
=======================================*/

/*	=======	M O S T R A R Promocion	=======*/ 
tblPromocion = $("#promocionBack").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/promocion.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	 "width": "20%", "orderable": false	},+
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Promocion	=======*/
$("#btnAgregarPromocion").on('click', function () {

	$("#frmRegistrarPromocion")[0].reset();

	$("#frmRegistrarPromocion").unbind('submit').bind('submit', function () {

		var formData = new FormData(document.getElementById("frmRegistrarPromocion"));
		var imagenPromo = $('#subirFoto')[0].files[0];
		formData.append('imagen', imagenPromo);

		$.ajax({
			url: "views/ajax/promocion.php?op=guardar",
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (response) {
				if (response.success == true){
					$("#frmRegistrarPromocion")[0].reset();
					tblPromocion.ajax.reload(null, false);
					$("#registrarPromocion").modal('hide');
					successSwal(response.mensaje);
				}else{
					$("#registrarPromocion").modal('hide');
					errorSwal(response.mensaje);
				}
			}
		});
		return false;
	});
});


/*	=======	E D I T A R Promocion	=======*/
function editarPromocion(idPromocion) {
	$("#frmEditarPromocion")[0].reset();
	if (idPromocion) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/promocion.php?op=getID",
			type: 'POST',
			data: {
				idPromocion: idPromocion
			},
			dataType: 'json',
			success: function (response) {

				$("#imagenPromoActual")
					.fileinput("destroy")
					.fileinput({

					theme:'fas',
					browseClass: "btn btn-primary btn-block",
					allowedFileExtensions: ['jpg', 'png'],
					browseLabel: "Examinar nueva imagen promocion...",
					maxFilesNum: 1,
					language: 'es',
					showCaption: false,
					showRemove: false,
					showUpload: false,
					showCancel: false,
					initialPreview: [
						`<img class='img-fluid' src='../img/promociones/2018/${response.img_peque_promo}'>`
					],
					initialPreviewConfig: [{
						caption: response.imagen_slide
					}]
				});
				$("#fotoAntigua").val(`../img/promociones/2018/${response.img_peque_promo}`);

				$("#id_promo").val(response.id_promo);
				$("#emarca").val(response.marca_promo);
				$("#etitulo").val(response.titulo_promo);
				$("#edescripcion").val(response.desc_promo);
				$("#elink1").val(response.link1);
				$("#elink2").val(response.link2);


				$("#frmEditarPromocion").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarPromocion"));
					var imagenPromo = $('#imagenPromoActual')[0].files[0];
					formData.append('imagenPromo', imagenPromo);
					//if (marca && titulo && descripcion) {
					$.ajax({
						url: "views/ajax/promocion.php?op=editar",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblPromocion.ajax.reload(null, false);
								$("#editarPromocion").modal('hide');
								successSwal(response.mensaje);

							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#editarPromocion").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
					//}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Promocion	=======*/
function activarPromocion(idPromocion = null){
	if(idPromocion){
		swal({
			title: "¿Realmente desea A C T I V A R esta promoción?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/promocion.php?op=activar",
					type: 'post',
					data: {idPromocion : idPromocion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblPromocion.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Promocion	=======*/
function desactivarPromocion(idPromocion = null){
	if(idPromocion){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta promocion?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/promocion.php?op=desactivar",
					type: 'post',
					data: {idPromocion : idPromocion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblPromocion.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}



/** Mensajes de Acierto y Error. */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}