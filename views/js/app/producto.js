

personalizarEditor('desclarga');
personalizarEditor('edesclarga');
personalizarEditor('especificaciones');
personalizarEditor('eespecificaciones');

function personalizarEditor(nombre){
	CKEDITOR.replace( nombre, {
		uiColor: '#E11414',
		height: 120,
		toolbarGroups: [{
			"name": "basicstyles",
			"groups": ["basicstyles"]
		  },
		  {
			"name": "paragraph",
			"groups": ["list"]
		  }	
		],
	});
}


$("#imagenProducto").fileinput({
	theme:'fas',
	browseClass: "btn btn-primary btn-block",
	allowedFileExtensions: ['jpg', 'png'],
	maxFilesNum: 1,
	language: 'es',
	minImageWidth: 250,
	minImageHeight: 250,
	maxImageWidth: 250,
	maxImageHeight: 250,
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false,
	browseLabel:"Examinar imagen del producto"
});


$("#pdfProducto").fileinput({
	theme:'fas',
	browseClass: "btn btn-info btn-block",
	allowedFileExtensions: ['PDF'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false,
	browseLabel:"Examinar PDF del producto"
});


var tblProducto;

/* =======================================
    AREA PARA OPERACIONES MARCA.
=======================================*/

/*	=======	M O S T R A R Marca	=======*/ 
tblProducto = $("#producto").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/producto.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}

	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Marca	=======*/
$("#btnAgregarProducto").on('click', function () {

	$("#frmRegistrarProducto")[0].reset();

	$("#frmRegistrarProducto").unbind('submit').bind('submit', function () {

		var formData = new FormData(document.getElementById("frmRegistrarProducto"));

		$.ajax({
			url: "views/ajax/producto.php?op=guardar",
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'json',
			success: function (response) {
				if (response.success == true){
					$("#frmRegistrarProducto")[0].reset();
					tblProducto.ajax.reload(null, false);
					$("#registrarProducto").modal('hide');
					successSwal(response.mensaje);
				}else{
					$("#registrarProducto").modal('hide');
					errorSwal(response.mensaje);
				}
			}
		});
		return false;
	});
});


/*	=======	E D I T A R Marca	=======*/
function editarProducto(idProducto) {
	$("#frmEditarProducto")[0].reset();
	if (idProducto) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/producto.php?op=getID",
			type: 'POST',
			data: {
				idProducto: idProducto
			},
			dataType: 'json',
			success: function (response) {

				$("#id_producto").val(response.id_prod);
				$("#eclave").val(response.clave_prod);
				$("#enombre").val(response.name_prod);
				$("#edesccorta").val(response.desc_corta_prod);
				CKEDITOR.instances.edesclarga.setData(response.desc_larga_prod);
				CKEDITOR.instances.eespecificaciones.setData(response.espec1_prod);
				$("#eespecificaciones").val(response.espec1_prod);
				$("#emarca").val(response.marca_prod);
				$("#esolucion").val(response.sol_prod);
				$("#eseccion").val(response.seccion_prod);
				$("#elinea").val(response.linea_prod);
				$("#eserie").val(response.serie_prod);
				$("#efamilia").val(response.familia_prod);

				$("#frmEditarProducto").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarProducto"));
					$.ajax({
						url: "views/ajax/producto.php?op=editar",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {
								tblProducto.ajax.reload(null, false);
								$("#editarProducto").modal('hide');
								successSwal(response.mensaje);
							} else {
								$("#editarProducto").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}



/*	=======	E D I T A R Marca	=======*/
function imagenProducto(idProducto) {
	$("#frmRegistrarImagenProducto")[0].reset();
	if (idProducto) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/producto.php?op=getID",
			type: 'POST',
			data: {
				idProducto: idProducto
			},
			dataType: 'json',
			success: function (response) {

				$("#idProductoImg").val(response.id_prod);
				$("#marcaProductoImg").val(response.marca_prod);

				$("#frmRegistrarImagenProducto").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmRegistrarImagenProducto"));
					var imagenProducto = $('#imagenProducto')[0].files[0];
					formData.append('imagen', imagenProducto);
					
					$.ajax({
						url: "views/ajax/producto.php?op=imagen",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblProducto.ajax.reload(null, false);
								$("#registrarImagenProducto").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#registrarImagenProducto").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					}); 
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}

function pdfProducto(idProducto) {
	$("#frmRegistrarPDFProducto")[0].reset();
	if (idProducto) {

		$.ajax({
			url: "views/ajax/producto.php?op=getID",
			type: 'POST',
			data: {
				idProducto: idProducto
			},
			dataType: 'json',
			success: function (response) {

				$("#idProductoPDF").val(response.id_prod);
				$("#marcaProductoPDF").val(response.marca_prod);

				$("#frmRegistrarPDFProducto").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmRegistrarPDFProducto"));
					var pdfProducto = $('#pdfProducto')[0].files[0];
					formData.append('pdf', pdfProducto);
					$.ajax({
						url: "views/ajax/producto.php?op=pdf",
						type: 'POST', // Es lo mismo a poner POST
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {

							if (response.success == true) {

								tblProducto.ajax.reload(null, false);
								$("#registrarPDFProducto").modal('hide');
								successSwal(response.mensaje);
							} else {
								//Cerrar ventana y mostrar mensaje de error.
								$("#registrarPDFProducto").modal('hide');
								errorSwal(response.mensaje);
							}
						}
					});
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	A C T I V A R  Marca	=======*/
function activarProducto(idProducto = null){
	if(idProducto){
		swal({
			title: "¿Realmente desea A C T I V A R este producto?",
			icon: "warning",
			buttons: true,
			successMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/marca.php?op=activar",
					type: 'post',
					data: {idProducto : idProducto},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblProducto.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}


/*	=======	D E S A C T I V A R  Marca	=======*/
function desactivarProducto(idProducto = null){
	if(idProducto){
		swal({
			title: "¿Realmente desea D E S A C T I V A R este producto?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/producto.php?op=desactivar",
					type: 'post',
					data: {idProducto : idProducto},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							tblProducto.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			}
		});
	}
}



/** Mensajes de Acierto y Error. */
function successSwal(mensaje) {
	swal({
		title: "Éxito!",
		text: mensaje,
		icon: "success",
	});	
}


function errorSwal(mensaje){
	swal({
		title: "Error",
		text: mensaje,
		icon: "error",
	});
}