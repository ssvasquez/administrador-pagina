
/*=============================================
Editar Item Slide 
=============================================*/

function editarSlide(idSlide) {
	
	$("#editarSlide").css("display", "block");;  // Mostrar div para editar slider
	$("#frmEditarSlide")[0].reset();
	if (idSlide) {
		$.ajax({
			url: "views/ajax/slide.php?op=getID",
			type: 'POST',
			data: {
				idSlide: idSlide
			},
			cache: false,
			dataType: 'json',
			success: function (response) {
				$("#imagenSlideActual")
					.fileinput("destroy")
					.fileinput({

					theme:'fas',
					browseClass: "btn btn-primary btn-block",
					allowedFileExtensions: ['jpg', 'png'],
					browseLabel: "Examinar nuevo slide...",
					maxFilesNum: 1,
					language: 'es',
					showCaption: false,
					showRemove: false,
					showUpload: false,
					showCancel: false,
					minImageWidth: 1170,
					minImageHeight: 390,
					initialPreview: [
						`<img class='img-fluid' src='../img/home/${response.imagen_slide}'>`
					],
					initialPreviewConfig: [{
						caption: response.imagen_slide
					}]
				});

				$("#fotoAntigua").val(`../img/home/${response.imagen_slide}`);
				$("#idSlide").val(response.id_slide);
				$("#emarca").val(response.marca_slide);
				$("#edescripcion").val(response.desc_slide);
				$("#eliga").val(response.liga_slide);

				$("#frmEditarSlide").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarSlide"));
					var imagenSlide = $('#imagenSlideActual')[0].files[0];
					formData.append('imagenSlide', imagenSlide);
					$.ajax({
						url: "views/ajax/slide.php?op=editar",
						type: 'POST', 
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						dataType: 'json',
						success: function (response) {
							if (response.success == true) {
								swal({
									title: "¡OK!",
									text: "¡El slide ha sido actualizado correctamente!",
									icon: "success",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false
								}).then((value) =>{
									window.location = "slide";
								})
							} else {
								errorSwal(response.mensaje);
							}
						}
					});
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}