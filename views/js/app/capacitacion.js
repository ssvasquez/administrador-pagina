$("#fotoCapacitacion").fileinput({
	theme:'fas',
	browseClass: "btn btn-primary btn-block",
	allowedFileExtensions: ['jpg', 'png'],
	maxFilesNum: 1,
	language: 'es',
	showCaption: false,
	showRemove: false,
	showUpload: false,
	showCancel: false
});


var tblCapacitacion;

/* =======================================
    AREA PARA OPERACIONES CAPACITACION.
=======================================*/

/*	=======	M O S T R A R Capacitacion	=======*/ 
tblCapacitacion = $("#capacitacion").DataTable({
	"lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todos"]],
	"ajax": "views/ajax/capacitacion.php?op=mostrar",
	"order": [],
	"columns": [
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	},
		{	"orderable": false	}
	],
	responsive: true,
	"language": {
		"url": "views/js/json/Spanish.json"
	},
	columnDefs: [
		{
			responsivePriority: 1,
			targets: 0
		},
		{
			responsivePriority: 2,
			targets: 0
		}
	]
});


/*	=======	A G R E G A R  Capacitacion	=======*/
$("#btnAgregarCapacitacion").on('click', function () {

	$("#frmRegistrarCapacitacion")[0].reset();

	$("#frmRegistrarCapacitacion").unbind('submit').bind('submit', function () {

		var formData = new FormData(document.getElementById("frmRegistrarCapacitacion"));
		var imagenCapacitacion = $('#fotoCapacitacion')[0].files[0];
		formData.append('imagen', imagenCapacitacion);

		//var datos = new FormData();
		//datos.append("imagen", imagen);

		//alert(imagen);
		//if (nuevoMarca && nuevoTitulo && nuevoDescripcion){
			
			$.ajax({
				url: "views/ajax/capacitacion.php?op=guardar",
                type: 'POST',
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
                dataType: 'json',
                success: function (response) {
					if (response.success == true){
						$("#frmRegistrarCapacitacion")[0].reset();
						tblCapacitacion.ajax.reload(null, false);
						$("#registrarCapacitacion").modal('hide');
						successSwal(response.mensaje);
					}else{
						$("#registrarCapacitacion").modal('hide');
						errorSwal(response.mensaje);
					}
				}
			});
		//}
		//errorSwal("Inserte datos en el formulario");
		return false;
	});
});


/*	=======	E D I T A R Capacitacion	=======*/
function editarCapacitacion(idCapacitacion) {
	$("#frmEditarCapacitacion")[0].reset();
	if (idCapacitacion) {
		
		// Buscar datos de la promo
		$.ajax({
			url: "views/ajax/capacitacion.php?op=getID",
			type: 'POST',
			data: {
				idCapacitacion: idCapacitacion
			},
			dataType: 'json',
			success: function (response) {

				$("#imagenCapacitacionActual")
					.fileinput("destroy")
					.fileinput({

					theme:'fas',
					browseClass: "btn btn-warning btn-block",
					allowedFileExtensions: ['jpg', 'png'],
					browseLabel: "Examinar nueva imagen capacitación...",
					maxFilesNum: 1,
					language: 'es',
					showCaption: false,
					showRemove: false,
					showUpload: false,
					showCancel: false,
					initialPreview: [
						`<img class='img-fluid' src='../${response.img_cap}'>`
					],
					initialPreviewConfig: [{
						caption: response.imagen_slide
					}]
				});
				$("#fotoAntigua").val(response.img_cap);
				$("#id_capacitacion").val(response.id_cap);
				$("#eciudad").val(response.lugar_cap);
				$("#emes").val(response.mes_cap);
				$("#efecha").val(response.fecha_cap);
				$("#ehora").val(response.hora_cap);
				$("#etema").val(response.tema_cap);
				$("#edescripcion").val(response.desc_cap);
				$("#emarca").val(response.marca_cap);
				$("#ecosto").val(response.costo_cap);
				$("#econtacto").val(response.contacto_cap);
				$("#ecorreo").val(response.correo_cap);
				$("#eliga").val(response.liga_cap);

				$("#frmEditarCapacitacion").unbind('submit').bind('submit', function () {
					var formData = new FormData(document.getElementById("frmEditarCapacitacion"));
					
					var imagenCapacitacion = $('#imagenCapacitacionActual')[0].files[0];
					formData.append('imagenCapacitacion', imagenCapacitacion);

					//if (marca && titulo && descripcion) {
						$.ajax({
							url: "views/ajax/capacitacion.php?op=editar",
							type: 'POST', 
							data: formData,
							cache: false,
							contentType: false,
							processData: false,
							dataType: 'json',
							success: function (response) {

								if (response.success == true) {

									tblCapacitacion.ajax.reload(null, false);
									$("#editarCapacitacion").modal('hide');
									successSwal(response.mensaje);

								} else {
									//Cerrar ventana y mostrar mensaje de error.
									$("#editarCapacitacion").modal('hide');
									errorSwal(response.mensaje);
								}
							}
						});
					//}
				return false;
				});
			}
		});
	} else {
		alert("Error: Refrescar la página de nuevo");
	}
}


/*	=======	D E S A C T I V A R  Capacitacion	=======*/
function desactivarCapacitacion(idCapacitacion = null){
	if(idCapacitacion){
		swal({
			title: "¿Realmente desea D E S A C T I V A R esta capacitación?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Aceptar"],
		}).then((willDelete) => {
			if (willDelete) {
				$.ajax({
					url:"views/ajax/capacitacion.php?op=desactivar",
					type: 'post',
					data: {idCapacitacion : idCapacitacion},
					dataType: 'json',
					success:function(response) {
						if(response.success == true) {
							// Actualizar la tabla, Cerrar modal y Mostrar mensaje de éxito.
							tblCapacitacion.ajax.reload(null, false);
							successSwal(response.mensaje);
						} else {
							errorSwal(response.mensaje);
						}
					}
				});
			} 
		});
	}
}