<?php

	require_once "db.php";

	class SucursalModels{

		//	mostrar Sucursal 
		public static function mostrarSucursalModel($tabla){
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}
		
		// Guardar nuevas Sucursales ...
		public static function guardarSucursalModel($datosModel, $tabla){
			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (nom_suc, calle_suc, num_suc, cp_suc, ciudad_suc, deleg_mun_suc, estado_suc, col_suc, map_suc, tel_suc, contacto_suc, correo_suc, horario_ent_suc,  horario_sal_suc,  horario_ent_sab_suc,  horario_sal_sab_suc, img_suc, img_suc_cel, alt_sucursal, estatus_suc, zona_suc) VALUES (:nombre, :calle, :numero, :cp, :ciudad, :delegacion, :estado, :colonia, :mapa, :telefono, :contacto, :correo, :horario_ent1, :horario_sal1, :horario_ent2, :horario_sal2, :imagen, :imagen2, :alt, :estatus, :zona)");

			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":calle", $datosModel['calle'], PDO::PARAM_STR);
			$stmt->bindParam(":numero", $datosModel['numero'], PDO::PARAM_STR);
			$stmt->bindParam(":cp", $datosModel['cp'], PDO::PARAM_STR);
			$stmt->bindParam(":ciudad", $datosModel['ciudad'], PDO::PARAM_STR);
			$stmt->bindParam(":delegacion", $datosModel['delegacion'], PDO::PARAM_INT);
			$stmt->bindParam(":estado", $datosModel['estado'], PDO::PARAM_STR);
			$stmt->bindParam(":colonia", $datosModel['colonia'], PDO::PARAM_STR);
			$stmt->bindParam(":mapa", $datosModel['mapa'], PDO::PARAM_STR);
			$stmt->bindParam(":telefono", $datosModel['telefono'], PDO::PARAM_STR);
			$stmt->bindParam(":contacto", $datosModel['contacto'], PDO::PARAM_STR);
			$stmt->bindParam(":correo", $datosModel['correo'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_ent1", $datosModel['horario_ent1'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_sal1", $datosModel['horario_sal1'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_ent2", $datosModel['horario_ent2'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_sal2", $datosModel['horario_sal2'], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":imagen2", $datosModel['imagen2'], PDO::PARAM_STR);
			$stmt->bindParam(":alt", $datosModel['alt'], PDO::PARAM_STR);
			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_STR);
			$stmt->bindParam(":zona", $datosModel['zona'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();
		}

		// Seleccionar una sucursal por ID
		public static function mostrarSucursalIDModel($datosModel, $tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_suc = :id");

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();
		}

		//	Habilitar / Deshabilitar sucursal
		public static function estatusSucursalModel($datosModel, $tabla){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estatus_suc = :estatus WHERE id_suc = :id");

			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_STR);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}

		//	Editar sucursal
		public static function editarSucursalModel($datosModel, $tabla){
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nom_suc = :nombre, calle_suc = :calle, num_suc = :numero, cp_suc = :cp, ciudad_suc = :ciudad, deleg_mun_suc = :delegacion, estado_suc = :estado, col_suc = :colonia, map_suc = :mapa, tel_suc = :telefono, contacto_suc = :contacto, correo_suc = :correo, horario_ent_suc = :horario_ent1,  horario_sal_suc = :horario_sal1,  horario_ent_sab_suc = :horario_ent2,  horario_sal_sab_suc = :horario_sal2, alt_sucursal = :alt, zona_suc = :zona WHERE id_suc = :id");

			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":calle", $datosModel['calle'], PDO::PARAM_STR);
			$stmt->bindParam(":numero", $datosModel['numero'], PDO::PARAM_STR);
			$stmt->bindParam(":cp", $datosModel['cp'], PDO::PARAM_STR);
			$stmt->bindParam(":ciudad", $datosModel['ciudad'], PDO::PARAM_STR);
			$stmt->bindParam(":delegacion", $datosModel['delegacion'], PDO::PARAM_INT);
			$stmt->bindParam(":estado", $datosModel['estado'], PDO::PARAM_STR);
			$stmt->bindParam(":colonia", $datosModel['colonia'], PDO::PARAM_STR);
			$stmt->bindParam(":mapa", $datosModel['mapa'], PDO::PARAM_STR);
			$stmt->bindParam(":telefono", $datosModel['telefono'], PDO::PARAM_STR);
			$stmt->bindParam(":contacto", $datosModel['contacto'], PDO::PARAM_STR);
			$stmt->bindParam(":correo", $datosModel['correo'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_ent1", $datosModel['horario_ent1'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_sal1", $datosModel['horario_sal1'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_ent2", $datosModel['horario_ent2'], PDO::PARAM_STR);
			$stmt->bindParam(":horario_sal2", $datosModel['horario_sal2'], PDO::PARAM_STR);			
			$stmt->bindParam(":alt", $datosModel['alt'], PDO::PARAM_STR);
			$stmt->bindParam(":zona", $datosModel['zona'], PDO::PARAM_STR);

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}		
	}
?>