<?php

	require_once "db.php";

	class CapacitacionModels{

		//	mostrar Capacitacion 
		public static function mostrarCapacitacionModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT id_cap, lugar_cap, mes_cap, fecha_cap, hora_cap, marca_cap, tema_cap, contacto_cap, correo_cap, status_cap FROM $tabla WHERE status_cap = 1 ORDER BY id_cap DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}


		// Guardar nuevas promociones ...
		public static function guardarCapacitacionModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (lugar_cap, mes_cap, fecha_cap, hora_cap, img_cap, desc_cap, marca_cap, tema_cap, costo_cap, contacto_cap, correo_cap, liga_cap, status_cap) VALUES (:lugar, :mes, :fecha, :hora, :imagen, :descripcion, :marca, :tema, :costo, :contacto, :correo, :liga, :estatus)");

			$stmt->bindParam(":lugar", $datosModel['lugar'], PDO::PARAM_STR);
			$stmt->bindParam(":mes", $datosModel['mes'], PDO::PARAM_STR);
			$stmt->bindParam(":fecha", $datosModel['fecha'], PDO::PARAM_STR);
			$stmt->bindParam(":hora", $datosModel['hora'], 	PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['desc'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":tema", $datosModel['tema'], 	PDO::PARAM_STR);
			$stmt->bindParam(":costo", $datosModel['costo'], PDO::PARAM_STR);
			$stmt->bindParam(":contacto", $datosModel['contacto'], PDO::PARAM_STR);
			$stmt->bindParam(":correo", $datosModel['correo'], PDO::PARAM_STR);
			$stmt->bindParam(":liga", $datosModel['liga'], 	PDO::PARAM_STR);
			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);

			return $stmt->execute();
	
			$stmt->close();

		}


		public static function mostrarCapacitacionIDModel($datosModel, $tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_cap = :id");

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();

		}


		public static function estatusCapacitacionModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET status_cap = :estatus WHERE id_cap = :id");

			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();

		}

		public static function editarCapacitacionModel($datosModel, $tabla){

			//lugar_cap = :lugar, mes_cap = :mes,
			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET fecha_cap = :fecha, hora_cap = :hora,  img_cap = :imagen, desc_cap = :descripcion, marca_cap = :marca, tema_cap = :tema, costo_cap = :costo, contacto_cap = :contacto, correo_cap = :correo, liga_cap = :liga WHERE id_cap = :id");

			$stmt->bindParam(":fecha", $datosModel['fecha'], PDO::PARAM_STR);
			$stmt->bindParam(":hora", $datosModel['hora'], 	PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], 	PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['desc'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":tema", $datosModel['tema'], 	PDO::PARAM_STR);
			$stmt->bindParam(":costo", $datosModel['costo'], PDO::PARAM_STR);
			$stmt->bindParam(":contacto", $datosModel['contacto'], PDO::PARAM_STR);
			$stmt->bindParam(":correo", $datosModel['correo'], PDO::PARAM_STR);
			$stmt->bindParam(":liga", $datosModel['liga'], 	PDO::PARAM_STR);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}
    }
?>