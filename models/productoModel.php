<?php

	require_once "db.php";

	class ProductoModels{

		//	mostrar Marca 
		public static function mostrarProductoModel($tabla){

			//$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla  WHERE status_prod = 1 ");
			$stmt = Conexion::conectar()->prepare("SELECT id_prod, clave_prod, sol_prod, seccion_prod, linea_prod, marca_prod, serie_prod, status_prod, titulo_prod_pdf, titulo_prod_img FROM $tabla LEFT JOIN imagen on clave_prod_img = id_prod LEFT JOIN pdf on clave_prod_pdf = id_prod WHERE (status_prod = '1' OR status_prod = '2')  ORDER BY id_prod DESC ");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}
		
		
		public static function mostrarMarcasModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT id, nombre FROM $tabla WHERE status = 1 or status = 2 ORDER BY nombre ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}


		// Guardar nueva marca ...
		public static function guardarProductoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (clave_prod, name_prod, desc_corta_prod, desc_larga_prod, espec1_prod, sol_prod, seccion_prod, linea_prod, marca_prod, serie_prod, familia_prod, status_prod) VALUES (:clave, :nombre, :desccorta, :desclarga, :especificacion, :solucion, :seccion, :linea, :marca, :serie, :familia, :estatus )");

			$stmt->bindParam(":clave", $datosModel['clave'], PDO::PARAM_STR);
			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":desccorta", $datosModel['desccorta'], PDO::PARAM_STR);
			$stmt->bindParam(":desclarga", $datosModel['desclarga'], PDO::PARAM_STR);
			$stmt->bindParam(":especificacion", $datosModel['especificacion'], PDO::PARAM_STR);
			$stmt->bindParam(":solucion", $datosModel['solucion'], PDO::PARAM_STR);
			$stmt->bindParam(":seccion", $datosModel['seccion'], PDO::PARAM_STR);
			$stmt->bindParam(":linea", $datosModel['linea'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":serie", $datosModel['serie'], PDO::PARAM_STR);
			$stmt->bindParam(":familia", $datosModel['familia'], PDO::PARAM_STR);
			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();
		}


		public static function guardarPDF($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (clave_prod_pdf, marca_pdf, titulo_prod_pdf) VALUES (:id_prod, :marcapdf, :titulopdf)");

			$stmt->bindParam(":id_prod", $datosModel['id_prod'], PDO::PARAM_STR);
			$stmt->bindParam(":marcapdf", $datosModel['marcapdf'], PDO::PARAM_STR);
			$stmt->bindParam(":titulopdf", $datosModel['titulopdf'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();
		}


		public static function guardarImagen($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (clave_prod_img, marca_img, titulo_prod_img) VALUES (:id_prod, :marcaimg, :tituloimg)");

			$stmt->bindParam(":id_prod", $datosModel['id_prod'], PDO::PARAM_STR);
			$stmt->bindParam(":marcaimg", $datosModel['marcaimg'], PDO::PARAM_STR);
			$stmt->bindParam(":tituloimg", $datosModel['tituloimg'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();			
		}


		public static function mostrarProductoIDModel($datosModel, $tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_prod = :id");

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();
		}


		public static function estatusProductoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET status_prod = :estatus WHERE id_prod = :id");

			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}

		public static function editarProductoModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET clave_prod = :clave, name_prod = :nombre,  desc_corta_prod = :desccorta,  desc_larga_prod= :desclarga, espec1_prod = :especificacion, sol_prod = :solucion, seccion_prod = :seccion, linea_prod = :linea, marca_prod = :marca, serie_prod = :serie, familia_prod = :familia WHERE id_prod = :id");

			$stmt->bindParam(":clave", $datosModel['clave'], PDO::PARAM_STR);
			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":desccorta", $datosModel['desccorta'], PDO::PARAM_STR);
			$stmt->bindParam(":desclarga", $datosModel['desclarga'], PDO::PARAM_STR);
			$stmt->bindParam(":especificacion", $datosModel['especificacion'], PDO::PARAM_STR);
			$stmt->bindParam(":solucion", $datosModel['solucion'], PDO::PARAM_STR);
			$stmt->bindParam(":seccion", $datosModel['seccion'], PDO::PARAM_STR);
			$stmt->bindParam(":linea", $datosModel['linea'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":serie", $datosModel['serie'], PDO::PARAM_STR);
			$stmt->bindParam(":familia", $datosModel['familia'], PDO::PARAM_STR);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}
    }
?>