<?php

require_once 'db.php';

class IngresoModels{

	public static function ingresoModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT id_usu, nombre_usu, pass_usu FROM $tabla WHERE nombre_usu = :usuario");

		$stmt -> bindParam(":usuario", $datosModel["usuario"], PDO::PARAM_STR);
		
		$stmt -> execute();

		return $stmt -> fetch(PDO::FETCH_ASSOC);

		$stmt -> close();

	}
}