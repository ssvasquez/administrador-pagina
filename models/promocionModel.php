<?php

	require_once "db.php";

	class PromocionModels{

		//	mostrar Promociones 
		public static function mostrarPromocionModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT id_promo, marca_promo, desc_promo, titulo_promo, estatus_promo, fecha_promo FROM $tabla WHERE estatus_promo = 1 ORDER BY id_promo DESC");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}

		public static function mostrarMarcasModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT id, nombre FROM $tabla WHERE status = 1 ORDER BY nombre ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}


		// Guardar nuevas promociones ...
		public static function guardarPromocionModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (nombre_promo, img_peque_promo, marca_promo, titulo_promo, desc_promo, estatus_promo, link1, link2, fecha_promo) VALUES (:nombre, :imagen, :marca, :titulo, :descripcion, :estatus, :link1, :link2, :fecha)");

			$stmt->bindParam(":nombre", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":titulo", $datosModel['titulo'], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['descripcion'], PDO::PARAM_STR);
			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);
			$stmt->bindParam(":link1", $datosModel['link1'], PDO::PARAM_STR);
			$stmt->bindParam(":link2", $datosModel['link2'], PDO::PARAM_STR);
			$stmt->bindParam(":fecha", $datosModel['fecha'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();

		}


		public static function mostrarPromocionIDModel($datosModel, $tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_promo = :id");

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();

		}


		public static function estatusPromocionModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estatus_promo = :estatus WHERE id_promo = :id");

			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();

		}


		public static function editarPromocionModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre_promo = :nombre, img_peque_promo = :promoimg, marca_promo = :marca, titulo_promo = :titulo, desc_promo = :descripcion, link1 = :link1, link2 = :link2 WHERE id_promo = :id");

			$stmt->bindParam(":promoimg", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":nombre", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":marca", $datosModel['marca'], PDO::PARAM_STR);
			$stmt->bindParam(":titulo", $datosModel['titulo'], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['descripcion'], PDO::PARAM_STR);
			$stmt->bindParam(":link1", $datosModel['link1'], PDO::PARAM_STR);
			$stmt->bindParam(":link2", $datosModel['link2'], PDO::PARAM_STR);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_STR);
	
			return $stmt->execute();
	
			$stmt->close();
		}
		
    }
?>