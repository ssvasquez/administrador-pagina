<?php 


require_once 'db.php';
date_default_timezone_set('America/Mexico_City');

class SlideModel{

	public static function mostrarSlideModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT id_slide, marca_slide, desc_slide, liga_slide, imagen_slide, status_slide FROM $tabla ORDER BY num_slide + 0 ASC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();
	}


	public static function mostrarSlideIDModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT id_slide, marca_slide, desc_slide, liga_slide, imagen_slide FROM $tabla WHERE id_slide = :id");

		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);
		
		$stmt -> execute();

		return $stmt -> fetch(PDO::FETCH_ASSOC);

		$stmt -> close();
	}

	public static function editarSlideModel($datosModel, $tabla){

		$hoy = date("Y-m-d H:i:s");
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET marca_slide = :marca, fecha_alta_slide = '$hoy', desc_slide = :descripcion, imagen_slide = :ruta, liga_slide = :liga WHERE id_slide = :id");	

		$stmt -> bindParam(":marca", $datosModel["marca"], PDO::PARAM_STR);
		$stmt -> bindParam(":descripcion", $datosModel["descripcion"], PDO::PARAM_STR);
		$stmt -> bindParam(":ruta", $datosModel["ruta"], PDO::PARAM_STR);
		$stmt -> bindParam(":liga", $datosModel["liga"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

		return $stmt->execute();	

		$stmt->close();
	}

	/*
	public static function actualizarOrdenModel($datos, $tabla){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET num_slide = :orden WHERE id_slide = :id");

		$stmt -> bindParam(":orden", $datos["ordenItem"], PDO::PARAM_STR);
		$stmt -> bindParam(":id", $datos["ordenSlide"], PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		}

		else{
			return "error";
		}

		$stmt -> close();

	}

		#SELECCIONAR ORDEN 
	#---------------------------------------------------

	public static function seleccionarOrdenModel($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT id_slide, liga_slide, marca_slide, desc_slide, imagen_slide FROM $tabla ORDER BY num_slide + 0 ASC");

		$stmt -> execute();

		return $stmt->fetchAll();

		$stmt->close();

	}*/

}