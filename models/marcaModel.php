<?php

	require_once "db.php";

	class MarcaModels{

		//	mostrar Marca 
		public static function mostrarMarcaModel($tabla){

			$stmt = Conexion::conectar()->prepare("SELECT id, nombre, imagen, descripcion, `status` FROM $tabla  WHERE `status` = 1 ORDER BY nombre ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

			$stmt -> close();
		}


		// Guardar nueva marca ...
		public static function guardarMarcaModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (nombre, imagen, descripcion, `status`) VALUES (:nombre, :imagen, :descripcion, :estatus)");

			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['descripcion'], PDO::PARAM_STR);
			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_STR);

			return $stmt->execute();
	
			$stmt->close();

		}


		public static function mostrarMarcaIDModel($datosModel, $tabla){
			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id = :id");

			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetch(PDO::FETCH_ASSOC);

			$stmt -> close();

		}


		public static function estatusMarcaModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET `status` = :estatus WHERE id = :id");

			$stmt->bindParam(":estatus", $datosModel['estatus'], PDO::PARAM_INT);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();

		}

		public static function editarMarcaModel($datosModel, $tabla){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, imagen = :imagen,  descripcion = :descripcion WHERE id = :id");

			$stmt->bindParam(":nombre", $datosModel['nombre'], PDO::PARAM_STR);
			$stmt->bindParam(":imagen", $datosModel['imagen'], 	PDO::PARAM_STR);
			$stmt->bindParam(":descripcion", $datosModel['descripcion'], PDO::PARAM_STR);
			$stmt->bindParam(":id", $datosModel['id'], PDO::PARAM_INT);
	
			return $stmt->execute();
	
			$stmt->close();
		}
    }
?>