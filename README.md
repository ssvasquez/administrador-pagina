# Administrador Pagina

Administrador para página coorporativa

## Tecnologías
<ul>
  <li>HTML5</li>
  <li>CSS3</li>
  <li>JavaScript</li>
  <li>PHP</li>
  <li>JSON</li>
  <li>AJAX</li>
  <li>jQuery</li>
  <li>MySQL</li>
</ul> 


## Librerías
<ul>
  <li>Bootstrap v4</li>
  <li>SweetAlert</li>
  <li>DataTables</li>
  <li>FileInput</li>
</ul>

---
layout: page
title: Documentación
---


##### <center> Manual de usuario para administración de sitio web </center>


### Tabla de contenido
* [Slider](#slider)
	* [Editar Slide](####editar-slide)
* [Capacitación](#capacitacion)
	* [Desactivar Capacitación](####desactivar-capacitacion)
	* [Editar Capacitación](####editar-capacitacion)
	* [Agregar Capacitación](####agregar-capacitacion)
* [Promociones](#promociones)
	* [Desactivar Promoción](####desactivar-promocion)
	* [Editar Promoción](####editar-promocion)
	* [Agregar Promoción](####agregar-promocion)
* [Marcas](#marcas)
	* [Desactivar Marca](####desactivar-marca)
	* [Editar Marca](####editar-marca)
	* [Agregar Marca](####agregar-marca)
* [Productos](#productos)
	* [Desactivar Producto](####desactivar-producto)
	* [Editar Producto](####editar-producto)
	* [Agregar Producto](####agregar-producto)
	* [Cambiar Imagen / PDF](####cambio-imagen-o-pdf)

---

## Slider

Del menú principal, `clic` en **Slider**, lo cual mostrará la siguiente lista con los banners activos en la página web.



#### Editar Slide

De la lista de banners `(12 en total)`, seleccionar la que se quiera cambiar, haciendo clic sobre el **lapiz** del mismo, con lo que mostrará el siguiente formulario.



Se realiza los cambios necesarios, ya sea cambiar un banner por completo o editar algún campo del formulario como es **marca**, **descripción** y/o **liga (URL)** del mismo.

Si se desea cambiar todo el banner, `clic` en el botón <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary">+ Examinar nuevo slide...</button>, seleccionar la nueva imagen, editar los campos de **Marca**, **Descripción** y **URL** (*Si el banner no cuenta con algún URL, dejarlo `vacío` o colocar el signo `#`*) y `clic` en el botón **Guardar Cambios**.


> **Nota:** *Al realizar los cambios para un banner, aveces no arroja la ventana de confirmación o de error, si esto ocurre, actualizar manualmente el navegador para ver el cambio*



<br>
<br>
<br>

## Capacitación

Del menú principal, clic en Capacitación. Al principio aparecen todas las capacitaciones activas en la web, como se muestra en la siguiente imagen:



#### Desactivar Capacitación

Dentro del listado de capacitación, `clic` en el botón <button style="background-color:#dc3545; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Desactivar </button>, sobre el elemento que se quiera dar de baja, confirmar la operación y con esto cambiará el estatus del mismo y ya no será visible en el área capacitación de la página web.

#### Editar Capacitación

Para editar una capacitación, `clic` en el botón <button style="background-color:#17a2b8; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Editar </button> del elemento que se quiera editar, con ello mostrará una ventana modal con el siguiente formulario, permitiendo cambiar la **_imagen_** del curso, así como el **_día_**, **_hora_**, **_tema_**, **_descripción_**, **_marca_**, **_costo_**, **_contacto_**, **_correo_** y **_liga_** del mismo. 
> No se puede editar la **_ciudad_** ni el **_mes_** del curso, en caso de requerir dichos cambios, desactivar el curso y dar de alta uno nuevo.

> **Nota:** *Regularmente la **Liga** del curso se aplica para los cursos en linea.*



#### Agregar Capacitación
Para dar de alta un nuevo curso, `clic` en el botón <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary">+ Agregar</button> y llenar todos los campos del formulario, para la mayoría de los casos el campo **Liga de Curso** deberá quedar `vacío` ya que éste aplica solamente para los cursos en línea.



<br>
<br>
<br>

## Promociones

Del menú principal, `clic` en Promociones. Al principio muestra una lista de las promociones activas en la página web.



#### Desactivar Promoción
Para desactivar una promoción, `clic` en el botón <button style="background-color:#dc3545; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Desactivar </button> del elemento que se quiera dar de baja, confirmar la acción y con con ello, cambiará el estatus del mismo y ya no será visible en la página de promociones.


#### Editar Promoción

Para editar una promoción, `clic` en el botón <button style="background-color:#17a2b8; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Editar </button>, lo cual mostrará el siguiente formulario, permitiendo cambiar la imagen de la promoción, así como la **_marca_**, **_titulo_**, **_descripción_** y **_links_** (en caso de tenerlos).



#### Agregar Promoción

Para dar de alta una nueva promoción, `clic` en <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary">+ Agregar</button> y llenar todos los campos del formulario, en la mayoría de los casos, los campos **_Link 1_** y **_Link 2_** se quedan vacíos, éstos solo se aplican cuando las imagenes cuentan con un área para dar clic.



<br>
<br>
<br>

## Marcas

Del menú principal, `clic` en Marcas. Al principio muestra una lista de las marcas activas en la página web. 



#### Desactivar Marca
Para desactivar una marca, `clic` en <button style="background-color:#dc3545; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Desactivar </button>, lo cual cambiará el estatus del mismo y ya no será visible en la página de marcas.

#### Editar Marca

Para editar una marca, `clic` en el botón <button style="background-color:#17a2b8; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary"> Editar </button>, lo cual mostrará el siguiente formulario, permitiendo cambiar la **_imagen_** de la marca, así el **_nombre_** y la **_descripción_** del mismo..



#### Agregar Marca

Para dar de alta una nueva marca, `clic` en <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary">+ Agregar</button> y llenar todos los campos del formulario.



<br>
<br>
<br>

## Productos

Del menú principal, `clic` en Productos. Al principio muestra una lista de los productos activos en la página web. 



#### Desactivar Producto
Para desactivar un producto, `clic` en <button style="background-color:#dc3545; color:#FFF; border:0; padding:5px; border-radius:5px" class="btn btn-primary"> Desactivar </button>, lo cual cambiará el estatus del mismo y ya no será visible en el sitio web.

#### Editar Producto

Para editar un producto, `clic` en el botón <button style="background-color:#17a2b8; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary"> Editar </button>, lo cual mostrará el siguiente formulario, permitiendo cambiar los detalles del producto.



#### Agregar Producto

Para dar de alta un nuevo producto, `clic` en <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary">+ Agregar</button> llenar los campos del formulario y clic en Registrar.



Para los campos **_Descripción Completa_** y **_Especificaciones_** se puede dar formato al contenido con las siguientes características: <br><br>
**Negrita** <br>
_Cursiva_ <br>
**_Negrita Cursiva_**<br>
Listado con viñetas:
- Elemento 1
- Elemento 2
- Elemento 3

Listado con numeración:
1. Elemento 1
2. Elemento 2
3. Elemento 3


Al terminar el registro de un nuevo producto, aparecerá los siguientes botones para agregar su `imagen` y `documento técnico` respectivamente.



Dar `clic` en el botón <button style="background-color:#007bff; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary"> Imagen </button>, enseguida aparece un modal para seleccionar la imagen y `clic` en **Subir Imagen**

Dar `clic` en el botón <button style="background-color:#ffc107; color:#FFF; border:0; padding:5px; border-radius:2px" class="btn btn-primary"> PDF </button>, enseguida aparece un modal para seleccionar el documento técnico *(.pdf)* `clic` en **Subir PDF**


#### Cambio Imagen o PDF

Para realizar el cambio de una `imagen` ó `PDF` de un producto, en esta versión se realiza manualmente, para ello:
- Extraer el nombre de la imagen o del PDF en la columna del `Imagen` o `PDF` del listado de productos.
- Reemplazar el nombre del archivo a subir.
- Desde FTP, subir *(reemplazar)* los archivos en las siguientes rutas:
	- **Para Imagen:** `img/marcas/productos/MarcaDelProducto/...`
	- **Para PDF:** `download/MarcaDelProducto/...`

